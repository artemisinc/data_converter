from setuptools import setup, find_packages
import glob
from pathlib import Path

ROOT_DIR = Path(__file__).absolute().parent

def get_scripts():
   return glob.glob(f'{ROOT_DIR}/*.py')


with open('requirements.txt') as f:
   requirements = f.read().splitlines()

setup(
   name='sdrparse',
   version='1.0',
   description='Python parsing scripts for SlimSDR and SlimSAR',
   license="MIT",
   long_description='Python parsing scripts for SlimSDR and SlimSAR',
   author='Jeff Budge',
   author_email='jbudge@artemisinc.net',
   url="http://www.foopackage.example/",
   packages=find_packages(),  #same as name
   install_requires=requirements, #external packages as dependencies
   scripts=get_scripts(),
)