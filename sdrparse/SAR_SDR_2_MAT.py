# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:32:24 2021

@author: Alex Zaugg and Jeff Budge
"""
from SDRParsing import SDRParse, isSAR
from SARParsing import SARParse
import tkinter as tk
import tkinter.filedialog as fd
from threading import Thread
import os
import scipy.io as sio
import struct
import numpy as np
import sys
import queue
import warnings
warnings.filterwarnings('ignore')

CAL_ATTENUATION = 31


def createList(r1, r2):
    return list(range(r1, r2 + 1))


class SAR2MAT(tk.Tk):
    # Class variables
    pathToFolder = ""
    fnme = ''
    destdir = ''

    numChannels = 0
    selectedChannel = 1
    parsedSAR = None
    parse_count = 0

    # Gets a directory path using the system's default file browser
    def getFile(self):
        # Get the path to a .sar file from the user
        tempdir = fd.askopenfilename(parent=self, initialdir=self.currdir,
                                     title='Select SAR or SDR file',
                                     filetypes=(('SAR files', '*.sar'),
                                                ('SDR files', '*.sdr')))
        # If the filename exists
        if not tempdir == "":
            self.pathToFolder = tempdir
            self.fnme = tempdir.split('/')[-1]
            self.showMessage("Click Parse")
            self.fileLabel.configure(text=self.fnme,
                                     background="lightgreen")
            self.currdir = tempdir[:-len(self.fnme)]

            # Adjust location variables
            self.parseFile.configure(state=tk.NORMAL)
            self.parsedLabel.configure(text="File Not Parsed",
                                       background="red")

    def getDest(self):
        # Get the path to a .sar file from the user
        tempdir = fd.askdirectory(parent=self, initialdir=self.destdir,
                                title='Select Folder')
        # If the filename exists
        if not tempdir == "":
            self.destdir = tempdir

    def checkQueue(self):
        try:
            self.parsedSAR = self.queue.get_nowait()
            self.updateAfterParse()
        except queue.Empty:
            self.parse_count += 1
            message = "Parsing"
            for n in range(self.parse_count):
                message += "."
            self.showMessage(message)
            self.after(500, self.checkQueue)

    # Parses the chosen .sar file
    def parse(self):
        # Change labels to "Parsing"
        self.parse_count = 0
        self.parsedLabel.configure(text="Parsing",
                                   background="yellow")
        self.parsedLabel.update()
        self.showMessage("Please wait for parsing to complete.\n")
        self.isSAR = isSAR(self.pathToFolder)
        self.parseFile.configure(state=tk.DISABLED)

        ParseThread(self.queue, self.pathToFolder).start()
        self.after(100, self.checkQueue)

    def updateAfterParse(self):
        # Set total channels once parsed
        self.numChannels = len(self.parsedSAR)

        # Update the labels to show the parsing has finsihed
        self.parsedLabel.configure(text="Parsing Complete",
                                   background="lightgreen")

        # Set up the list options
        inc = 0
        self.dataTypes = []
        for i in range(self.numChannels):
            if self.parsedSAR[i].ncals > 0:
                if self.isSAR:
                    self.dataTypes.append(
                        (inc, "Cal_Data_Channel_{}_Band_".format(i + 1) + self.parsedSAR[i].band_num[-1]))
                else:
                    self.dataTypes.append(
                        (inc, "Cal_Data_Channel_{}_".format(i + 1) + self.parsedSAR[i].xml['Freq_Band']))
            inc += 1
        for i in range(self.numChannels):
            if self.isSAR:
                self.dataTypes.append(
                    (inc, "Radar_Data_Channel_{}_Band_".format(i + 1) + self.parsedSAR[i].band_num[-1]))
            else:
                self.dataTypes.append(
                    (inc, "Radar_Data_Channel_{}_".format(i + 1) + self.parsedSAR[i].xml['Freq_Band']))
            inc += 1
        for i in range(self.numChannels):
            self.dataTypes.append((inc, "Interp_GPS_Channel_{}".format(i + 1)))
            inc += 1
        for i in range(self.numChannels):
            self.dataTypes.append((inc, "Ref_Chirp_Channel_{}".format(i + 1)))
            inc += 1
        for i in range(self.numChannels):
            self.dataTypes.append((inc, "Interp_Gimbal_Channel_{}".format(i + 1)))
            inc += 1
        self.dataTypes.append((inc + 1, "BESTPOS"))
        self.dataTypes.append((inc + 2, "INSPVAS"))
        self.dataTypes.append((inc + 3, "INSCOVS"))
        self.dataTypes.append((inc + 4, "TIMESYNC"))
        self.dataTypes.append((inc + 5, "PPS_Time"))
        self.dataTypes.append((inc + 6, "Gimbal"))
        self.selectionList.delete(0, tk.END)
        for d in self.dataTypes:
            self.selectionList.insert(*d)

        self.selectionList.bind('<<ListboxSelect>>', lambda x: self.showMessage('Click Generate.'))

        self.genMatFile.configure(state=tk.NORMAL)
        self.showMessage("Select desired outputs.")

        # Let them parse again if they feel like it
        self.parseFile.configure(state=tk.NORMAL)

    def showMessage(self, msg):
        self.messageBox.configure(state=tk.NORMAL)
        self.messageBox.delete(1.0, tk.END)
        self.messageBox.insert("end", msg, ('stdout',))
        self.messageBox.configure(state=tk.DISABLED)

    # Create the output files based on selections
    def generate(self):

        # If statements to run the write functions for each selection
        for i in self.selectionList.curselection():
            selection = self.dataTypes[i][1]
            dt = self.mode.get()
            data_type = '.mat' if dt == 0 else '.dat'
            fnme = self.destdir + '/' + self.fnme.split('.')[0] + '_' + selection + data_type
            self.showMessage('Generating ' + selection)
            if "Ref_Chirp" in selection:
                chirp = self.parsedSAR[int(selection.split('_')[3]) - 1].ref_chirp
                if dt == 0:
                    sio.savemat(fnme, {'data': data})
                else:
                    with open(fnme, 'wb') as f:
                        f.write(struct.pack('<i', len(chirp)))
                        if self.isSAR:
                            (chirp / 10 ** (CAL_ATTENUATION / 20)).astype('int16').tofile(f)
                        else:
                            (chirp / 10 ** (CAL_ATTENUATION / 20)).astype(np.complex64).tofile(f)
            if "Cal_Data" in selection:
                sel_channel = int(selection.split('_')[3]) - 1
                chan = self.parsedSAR[sel_channel]
                data = self.parsedSAR.getPulses(chan.cal_num, sel_channel, True)
                if dt == 0:
                    sio.savemat(fnme, {'data': data})
                else:
                    with open(fnme, 'wb') as f:
                        f.write(struct.pack('<i', chan.cal_num[-1]))
                        f.write(struct.pack('<i', chan.nsam))
                        np.array(np.ones((chan.cal_num[-1],)) * 31).astype('int8').tofile(f)
                        np.array(np.ones((chan.cal_num[-1],))).astype('double').tofile(f)
                        for d in range(data.shape[1]):
                            if self.isSAR:
                                (data[:, d] / 10 ** (CAL_ATTENUATION / 20)).astype('int16').tofile(f)
                            else:
                                (data[:, d] / 10 ** (CAL_ATTENUATION / 20)).view('(2,)float').flatten().astype(
                                    'int16').tofile(f)
            if "Radar_Data" in selection:
                sel_channel = int(selection.split('_')[3]) - 1
                if dt == 0:
                    for data in self.parsedSAR.getPulseGen(1000, sel_channel):
                        tmp_fnme = fnme[:-4] + '_{}.mat'.format(data[1][0])
                        sio.savemat(tmp_fnme, {'data': data[0], 'sys_times': data[3], 'attenuation': data[2]})
                else:
                    with open(fnme, 'wb') as f:
                        chan = self.parsedSAR[sel_channel]
                        total = chan.nframes
                        f.write(struct.pack('<i', chan.nframes))
                        f.write(struct.pack('<i', chan.nsam))
                        np.array(chan.atts).astype('int8').tofile(f)
                        np.array(chan.sys_time).astype('double').tofile(f)
                        for data in self.parsedSAR.getPulseGen(1000, sel_channel):
                            for d in range(data[0].shape[1]):
                                if not self.isSAR or (self.isSAR and chan.is_dechirp):
                                    (data[0][:, d] / 10 ** (data[2][d] / 20)).view('(2,)float').flatten().astype(
                                        'int16').tofile(f)
                                else:
                                    (data[0][:, d] / 10 ** (data[2][d] / 20)).astype('int16').tofile(f)
                            self.showMessage('Generating ' + selection + ': {}%'.format(
                               int(data[1][-1] / total * 100)))
                            self.messageBox.update()
            if selection == "BESTPOS":
                if dt == 0:
                    sio.savemat(fnme, {name: col.values for name, col in self.parsedSAR.bestpos.items()})
                else:
                    try:
                        bpos = self.parsedSAR.bestpos
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', bpos.shape[0]))
                            for col in bpos.columns:
                                np.array(bpos[col].values).astype('float64').tofile(f)
                    except:
                        self.showMessage("No BESTPOS found.")
            if selection == "INSCOVS":
                if dt == 0:
                    sio.savemat(fnme, {name: col.values for name, col in self.parsedSAR.inscovs.items()})
                else:
                    try:
                        bpos = self.parsedSAR.inscovs
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', bpos.shape[0]))
                            for col in bpos.columns:
                                try:
                                    np.array(bpos[col].values).astype('float64').tofile(f)
                                except ValueError:
                                    for row in bpos[col]:
                                        np.array(row).astype('float64').tofile(f)
                    except:
                        self.showMessage("No INSCOVS found.")

            if selection == "TIMESYNC":
                try:
                    if dt == 0:
                        sio.savemat(fnme, {name: col.values for name, col in self.parsedSAR.timesync.items()})
                    else:
                        bpos = self.parsedSAR.timesync
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', bpos.shape[0]))
                            for col in bpos.columns:
                                np.array(bpos[col].values).astype('float64').tofile(f)
                except:
                    self.showMessage("No TIMESYNC found.")
            if selection == "INSPVAS":
                try:
                    if dt == 0:
                        sio.savemat(fnme, {name: col.values for name, col in self.parsedSAR.gps_data.items()})
                    else:
                        bpos = self.parsedSAR.gps_data
                        bpos['gps_ms'] = bpos.index
                        bpos = self.parsedSAR.gps_data[['lat', 'lon', 'alt', 'vn', 've', 'vu', 'r', 'p',
                                                        'heading', 'gps_ms', 'systime']]
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', bpos.shape[0]))
                            for col in bpos.columns:
                                np.array(bpos[col].values).astype('float64').tofile(f)
                except:
                    self.showMessage("No INSPVAS found.")
            if selection == "PPS_Time":
                try:
                    if dt == 0:
                        sio.savemat(fnme, {'sys_time': self.parsedSAR.gps_data['systime'].values})
                    else:
                        bpos = self.parsedSAR.gps_data[['systime']]
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', bpos.shape[0]))
                            for col in bpos.columns:
                                np.array(bpos[col].values).astype('float64').tofile(f)
                except:
                    self.showMessage("No PPSTIME found.")
            if selection == "Gimbal":
                try:
                    if dt == 0:
                        sio.savemat(fnme, {name: col.values for name, col in self.parsedSAR.gimbal.items()})
                    else:
                        bpos = self.parsedSAR.gimbal[['pan', 'tilt', 'systime']]
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', bpos.shape[0]))
                            for col in bpos.columns:
                                np.array(bpos[col].values).astype('float64').tofile(f)
                except:
                    self.showMessage("No gimbal information found.")
            if 'Interp_GPS' in selection:
                try:
                    sel_channel = int(selection.split('_')[3]) - 1
                    bpos = self.parsedSAR.gps_data
                    bpos['gps_ms'] = bpos.index
                    bpos = self.parsedSAR.gps_data[['lat', 'lon', 'alt', 'vn', 've', 'vu', 'r', 'p',
                                                    'heading', 'gps_ms', 'systime']]
                    npos = {}
                    orig_gps = bpos['systime'].values
                    if len(orig_gps) > 0:
                        for col in bpos.columns:
                            npos[col] = np.interp(self.parsedSAR[sel_channel].sys_time, orig_gps, bpos[col])
                    else:
                        for col in bpos.columns:
                            npos[col] = 0

                    if dt == 0:
                        sio.savemat(fnme, npos)
                    else:
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', self.parsedSAR[sel_channel].nframes))
                            for col in npos:
                                np.array(npos[col]).astype('float64').tofile(f)
                except:
                    self.showMessage("No GPS data found.")
            if 'Interp_Gimbal' in selection:
                sel_channel = int(selection.split('_')[3]) - 1
                try:
                    bpos = self.parsedSAR.gimbal[['pan', 'tilt', 'systime']].astype(np.float64)
                    npos = {}
                    orig_gps = bpos['systime'].values
                    if len(orig_gps) > 0:
                        for col in bpos.columns:
                            npos[col] = np.interp(self.parsedSAR[sel_channel].sys_time, orig_gps, bpos[col])
                    else:
                        for col in bpos.columns:
                            npos[col] = 0
                    if dt == 0:
                        sio.savemat(fnme, npos)
                    else:
                        with open(fnme, 'wb') as f:
                            f.write(struct.pack('<i', len(self.parsedSAR[sel_channel].sys_time)))
                            for col in npos:
                                np.array(npos[col]).astype('float64').tofile(f)
                except:
                    self.showMessage("No gimbal information found.")

            # Once each selection has been written change labels
            self.showMessage("Output files placed in " + self.destdir)

    def setMode(self):
        print('Mode is ' + str(self.mode.get()))

    def __init__(self):
        super().__init__()
        self.destdir = os.getcwd()
        self.isSAR = False
        self.title('SAR Parse')
        self.configure(height=250, width=400)
        self.resizable(width=False, height=False)
        self.queue = queue.Queue()

        # Main widgets
        self.parseFile = tk.Button(self, state=tk.DISABLED)
        self.genMatFile = tk.Button(self, state=tk.DISABLED)
        destButton = tk.Button(self)
        self.getSAR = tk.Button(self)
        self.fileLabel = tk.Label(self)
        self.parsedLabel = tk.Label(self)
        self.selectionList = tk.Listbox(self)
        self.messageBox = tk.Text(self, wrap='word')
        self.messageBox.tag_configure('stdout')
        # sys.stdout = TextRedirector(self.messageBox, 'stdout')
        fileTypeLabel = tk.Label(self)
        self.dataTypes = []

        # Configure widgets
        self.parseFile.configure(text="Parse File",
                                 command=lambda: self.parse())
        self.genMatFile.configure(text="Generate",
                                  command=lambda: self.generate())
        self.getSAR.configure(text="Select File",
                              command=lambda: self.getFile())
        destButton.configure(text='Select Output Folder', command=lambda: self.getDest())
        self.fileLabel.configure(text="No File Selected",
                                 background="red")
        self.parsedLabel.configure(text="File Not Parsed",
                                   background="red")
        self.selectionList.configure(selectmode=tk.MULTIPLE)
        self.messageBox.configure(state=tk.DISABLED,
                                  background="lightgrey")
        fileTypeLabel.configure(text='Output Type:')
        self.modeSelect = []
        self.mode = tk.IntVar()
        self.mode.set(1)
        self.currdir = os.getcwd()
        for idx, mode in enumerate([('.MAT', 0),
                     ('.DAT', 1)]):
            self.modeSelect.append(tk.Radiobutton(self, text=mode[0], variable=self.mode,
                                                  command=lambda: self.setMode(), value=mode[1]))

        # All GUI objects
        self.genMatFile.pack()
        self.parseFile.pack()
        self.getSAR.pack()
        self.selectionList.pack()
        self.fileLabel.pack()
        self.parsedLabel.pack()
        self.messageBox.pack()
        destButton.pack()
        fileTypeLabel.pack()
        for obj in self.modeSelect:
            obj.pack()

        # Buttons
        self.getSAR.place(bordermode=tk.OUTSIDE, height=25, width=200)
        self.genMatFile.place(bordermode=tk.OUTSIDE, height=25,
                              width=400, y=25)
        self.parseFile.place(bordermode=tk.OUTSIDE, height=25,
                             width=200, x=200)
        destButton.place(bordermode=tk.OUTSIDE, height=25,
                         width=200, x=200, y=150)

        # Option List
        self.selectionList.place(bordermode=tk.OUTSIDE, height=200,
                                 width=200, y=50)

        # labels
        self.fileLabel.place(bordermode=tk.OUTSIDE, height=25,
                             width=200, x=200, y=50)
        self.parsedLabel.place(bordermode=tk.OUTSIDE, height=25,
                               width=200, x=200, y=75)
        self.messageBox.place(bordermode=tk.OUTSIDE, height=75,
                              width=200, x=200, y=175)
        fileTypeLabel.place(bordermode=tk.OUTSIDE, height=25, width=200, x=200, y=100)

        # Radio Buttons
        for idx, obj in enumerate(self.modeSelect):
            obj.place(bordermode=tk.OUTSIDE, height=25, width=50, x=250 + 50 * idx, y=125)

        self.showMessage('Select file to begin.')

        # Start the main loop that manages the GUI
        self.mainloop()


class ParseThread(Thread):
    def __init__(self, queue, pathToFolder):
        super().__init__()
        self.queue = queue
        self.path = pathToFolder
        self.parsedSAR = None

    def run(self):
        sar = isSAR(self.path)
        if sar:
            self.parsedSAR = SARParse(self.path)
        else:
            self.parsedSAR = SDRParse(self.path)
        self.queue.put(self.parsedSAR)


class TextRedirector(object):
    def __init__(self, widget, tag="stdout"):
        self.widget = widget
        self.tag = tag

    def write(self, str):
        self.widget.configure(state="normal")
        self.widget.insert("end", str, (self.tag,))
        self.widget.configure(state="disabled")


# Instance class and run main code
Example = SAR2MAT()
