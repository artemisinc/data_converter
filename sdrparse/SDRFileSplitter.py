"""
@brief This script takes a .sar file and creates several smaller .sar files
    that all contain the original calibration data.
@date 09/13/2024
@param fileName The name of the file being split.
@param splitfile_sizeGB the desired size of the split files
@returns N split files of user selected size with the calibration data
    of the original file.
@authors: Diego Clavijo <dclavijo@artemisinc.net>
  John Rocco <jrocco@artemisinc.net>
"""

import argparse
import math
import mmap
from pathlib import Path
import shutil
import sys
import xml.etree.ElementTree as ET

# CONSTANTS
BYTES_TO_MEGABYTES = 1000000
COLLECTION_MODE_OPERATION_SHIFT = 59
COLLECTION_MODE_OPERATION_MASK = 0x1800000000000000
DATA_START_WORD = b'\x0A\xB0\xFF\x18'
DEFAULT_OVERLAP_SIZE_B = 100000000
GPS_START_WORD = b'\x0A\xFF\xB0\x18'
GIMBAL_START_WORD = b'\x0A\xFF\x18\xB0'
NUM_BYTES_PER_SAMPLE = 4
NUM_BYTES_PER_WAVEFORM_SAMPLE = 2
NUM_OVERLAP_SECONDS = 2
SDR_HEADER_SZ = 28
STOP_WORD = b'\xAA\xFF\x11\x55'
TALON_HEADER = b'\x00\xA0\x57\x27'
WAVEFORM_START_WORD = b'\x0A\xB0\xFF\x45'

def copyAndRenameFile( a_originalFile, a_numCopies ) :
    originalFilePath = Path( a_originalFile )
    with open( a_originalFile, 'rb' ) as a_originalFile:
        for i in range( a_numCopies ) :
            newFileName = originalFilePath.stem + \
                f"_{i:02}" + originalFilePath.suffix
            shutil.copy( originalFilePath, newFileName )

def isSAR( a_filename ) :
    if '.sar' in a_filename:
        return True
    else:
        return False

def getFilePath( a_isSAR, a_filename ) :
    if a_isSAR :
        return Path( a_filename )
    else:
        return 'NULL'

def readDataHeader( a_data ) :
    numSamples = \
      int.from_bytes( a_data[ 16:20 ], byteorder='big', signed=False )
    return numSamples

if __name__ == '__main__' :
    # Read command line arguments
    parser = argparse.ArgumentParser()
    # Add the command line argument: fileName
    parser.add_argument( \
      "fileName", help="The name of the .sar file being split." )
    # Add the command line argument: splitFileSizeGB
    parser.add_argument( \
      "splitFileSizeGB",
      help="The size of files that will be created by the file splitter in GB",
      type=int )

    # Store the command line arguments
    args = parser.parse_args()

    # Check the file exists and is a .sar file
    if( not isSAR( args.fileName ) ) :
        print( "This program expects a .SAR file." )
        sys.exit( 1 )

    fileNamePath = Path( args.fileName )
    if( not fileNamePath.exists() ) :
        print( "Could not locate the .SAR file." )
        sys.exit( 1 )

    overlapSizeB = DEFAULT_OVERLAP_SIZE_B
    totalDataRateMBPS = 0
    XMLFileName = args.fileName.replace( ".sar", ".xml" )
    if( Path( XMLFileName ).exists() ) :
      tree = ET.parse( XMLFileName )
      root = tree.getroot()
      for dataRateTag in root.findall( './/Data_Rate_MBPS' ) :
        dataRateMBPS = float( dataRateTag.text )
        totalDataRateMBPS += dataRateMBPS
      overlapSizeB = \
        totalDataRateMBPS * NUM_OVERLAP_SECONDS * BYTES_TO_MEGABYTES
    else :
      print( "Count not found XML file." )
      sys.exit( 1 )

    # Set size of memory map to 2GB
    mmapSize = 2 * ( 2**30 )
    # File size
    fileSize = fileNamePath.stat().st_size
    # Split file size
    splitFileSizeB = args.splitFileSizeGB * 2**30
    splitFileSizeB -= overlapSizeB
    if( fileSize < splitFileSizeB ) :
        print( \
          "The split file size must be less than the original file size." )
        sys.exit( 1 )
    # Number of files to create
    splitCount = math.ceil( fileSize / splitFileSizeB )
    # Open the .sar file
    with fileNamePath.open( mode="r+b" ) as file:
        mm = mmap.mmap( file.fileno(), mmapSize )
        l_waveformPos = []
        l_calPos = []
        print( "Parsing..." )
        offset = 0
        # Find the waveforms
        while True:
          waveformPos = mm.find( WAVEFORM_START_WORD, offset )
          dataPos = mm.find( DATA_START_WORD, offset )
          if dataPos < waveformPos:
            break
          l_waveformPos.append( waveformPos )
          offset = waveformPos + len( WAVEFORM_START_WORD )

        # Find the cal data by reading the mode
        while True :
            mm.seek( mm.find( DATA_START_WORD ) )
            endOfCalDataPos = mm.tell()
            mode = mm.read( 20 )
            isCal = \
                ( int.from_bytes(mode, byteorder='big') &
                 COLLECTION_MODE_OPERATION_MASK ) \
                >> COLLECTION_MODE_OPERATION_SHIFT
            if( isCal != 1 ) :
                mm.close()
                break
            else :
              l_calPos.append( endOfCalDataPos )

        # Copy the data version, system code and cal data to a new file
        print( "Building split files..." )
        for x in range( splitCount ):
            endOfCalData = endOfCalDataPos
            newFile = fileNamePath.stem + f"_{x:02}" + fileNamePath.suffix
            if( x == 0 ) :
                with open( newFile, "w+b" ) as splitFile:
                    file.seek( 0, 0 )
                    while endOfCalData:
                        chunk = min( 1024*1024, endOfCalData )
                        data = file.read( chunk )
                        splitFile.write( data )
                        endOfCalData -= chunk
            else :
                with open( newFile, "w+b" ) as splitFile:
                    mm = mmap.mmap( file.fileno(), endOfCalDataPos )
                    # Read the header and skip if Talon header found
                    header = mm.read( 4 )
                    if( header == TALON_HEADER ):
                        talonHeaderData = mm.read( 1020 )
                    else:
                        mm.seek( 0 )
                    # Read and write the data version and system code
                    dataVersion = mm.read( 1 )
                    systemCode = mm.read( 1 )
                    splitFile.write( dataVersion )
                    splitFile.write( systemCode )
                    i = 0
                    while( i < len( l_waveformPos ) ) :
                        mm.seek( l_waveformPos[ i ] )
                        startWord = mm.read( 4 )
                        waveformNum = mm.read( 1 )
                        numberOfSamplesBytes = mm.read( 4 )
                        numberOfSamples = \
                          int.from_bytes( \
                            numberOfSamplesBytes, byteorder='big', \
                            signed=False )
                        data = \
                          mm.read( numberOfSamples * NUM_BYTES_PER_WAVEFORM_SAMPLE )
                        stopWord = mm.read( 4 )
                        if( stopWord == STOP_WORD ) :
                          splitFile.write( startWord )
                          splitFile.write( waveformNum )
                          splitFile.write( numberOfSamplesBytes )
                          splitFile.write( data )
                          splitFile.write( stopWord )
                        i += 1
                    i = 0
                    while ( i < len( l_calPos ) ):
                        mm.seek( l_calPos[ i ] )
                        startWord = mm.read( 4 )
                        headerData = mm.read( SDR_HEADER_SZ )
                        numberOfSamples = readDataHeader( headerData )
                        data = \
                          mm.read( numberOfSamples * NUM_BYTES_PER_SAMPLE )
                        stopWord = mm.read( 4 )
                        if( stopWord == STOP_WORD ):
                            splitFile.write( startWord )
                            splitFile.write( headerData )
                            splitFile.write( data )
                            splitFile.write( stopWord )
                        i += 1
            mm.close()
            # Determine remaining size of the split files
            startOfSplitFile = endOfCalDataPos + ( splitFileSizeB * x )
            endOfSplitFile = endOfCalDataPos + \
                ( splitFileSizeB * ( x + 1 ) ) + overlapSizeB
            size = endOfSplitFile - startOfSplitFile
            # Split the remaining data ( account for offset )
            with open( newFile, "a+b" ) as splitFile:
                file.seek( int( startOfSplitFile ) )
                while size:
                    chunk = min( 1024*1024, size )
                    data = file.read( chunk )
                    splitFile.write( data )
                    size -= chunk

    print( "Copying XML..." )
    copyAndRenameFile( XMLFileName, splitCount )

    print("Done")
