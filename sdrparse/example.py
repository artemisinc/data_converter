import numpy as np
import matplotlib.pyplot as plt
from aps_io import getRawDataGen, getRawData, loadGPSData, loadGimbalData, \
    loadXMLFile, getBasebandCenterFrequency, loadReferenceChirp, \
    loadMatchedFilter, window_taylor

def findPowerOf2(x):
    if x < 0:
        return 0
    else:
        x |= x >> 1
        x |= x >> 2
        x |= x >> 4
        x |= x >> 8
        x |= x >> 16
        return x + 1

def db(data):
    db_data = abs(data)
    try:
        db_data[db_data == 0] = 1e-15
    except TypeError:
        if db_data == 0:
            db_data = 1e-15
    return 20 * np.log10(db_data)


# System constants
fs = 2e9  # Sampling frequency
TAC = 125e6  # Clock frequency
c0 = 299792458.0  # Speed of light in m/s

# .SAR filename to get debug outputs
fnme = 'SAR_08052021_104324'

# Directory for debug outputs
debug_dir = '/data5/SAR_Freq_Data/08052021'

# XML file associated with .SAR file
xml_fnme = '/data2/RAW/08052021/SAR_08052021_104324.xml'

# Number of pulses in CPI
cpi_length = 512
# The Doppler interpolation factor
dop_interp_factor = 2
# The range interpolation factor
range_interp_factor = 1

# Parse XML data into dict
xml_data = loadXMLFile( xml_fnme, keep_structure=True )

# Get some relevant parameters from the XML
bandwidth = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Bandwidth_Hz']
vel = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Common_Channel_Settings']['Velocity_Knots']
pulse_length = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Pulse_Length_S']
is_upper_band = True if xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Upper_Band_Enabled'] == 'true' else False
nco_hz = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['NCO_Hz']
fc = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Center_Frequency_Hz']
band = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Freq_Band']
meters_per_bin = c0 / fs / range_interp_factor
dopp_prf = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Common_Channel_Settings']['Transmit_PRF_Hz'] / \
           xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Presum_Factor']

# Load in reference chirp to make the matched filter (this is made from
#   calibration data and is for matched filter creation)
ref_chirp = loadReferenceChirp(debug_dir + '/' + fnme + '_Ref_Chirp_Channel_1.dat')
num_samples = ref_chirp.shape[ 0 ]

# Compute the pulse length in samples
pulse_length_N = int( np.ceil( pulse_length * fs ) )
# Compute the full convolution length
convolution_length_N = num_samples + pulse_length_N - 1 
# Compute the number of complete convolution bins in the circular convolution
num_full_conv_bins = ( num_samples - pulse_length_N + 1 ) * range_interp_factor
# Get the proper length FFT to use based on the length of the Artemis matched
#   filter
fft_len = findPowerOf2( convolution_length_N )
ifft_len = fft_len * range_interp_factor
# NOTE: You can uncomment this next line if you want to see the entire ciruclar
#   convolution.  The near range bins with partial convolution will show up 
#   in the far range. 
#num_full_conv_bins = ifft_len
# Calculate basebanded center frequency
fc_baseband = getBasebandCenterFrequency(
    band, fc, nco_hz, is_upper_band=is_upper_band )
# Compute the near and far ranges
near_range =\
    ( ( xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Receive_On_TAC'] \
       - xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_0']['Transmit_On_TAC'] ) \
            / TAC ) * c0 / 2
far_range = near_range + num_full_conv_bins * meters_per_bin / 2.0

""" Basic steps for generating a simple matched filter using a reference chirp """
# Get the index at which the chirp actually starts in the reference pulse. The
#   more accurate this is, the more accurate the ranges will be, otherwise there
#   will be an offset related to the difference and the sampling rate.
# NOTE: 120 is pretty close to what it normally is for a 2 GHz sampling rate,
#   but this is just an approximation.
chirp_start_ind = 120
# Calculate the number of FFT bins wide the spectrum is (divided by two)
band_sz = int( bandwidth * fft_len / fs ) // 2
# Get the index at which the center of the spectrum should be
fc_baseband_bin = int( fc_baseband * fft_len / fs ) // 2
simple_match_filter =\
    np.fft.fft( ref_chirp[ chirp_start_ind: ], fft_len ).conj().T
# Generate a window for sidelobe control
upper_len = fc_baseband_bin + band_sz
lower_len = band_sz * 2 - upper_len
range_window = window_taylor( band_sz * 2, 5, -35.0 )
# Place the window in the correct place with relation to the spectrum and let
#   the rest be zeros
rc_window = np.zeros( fft_len )
rc_window[ :upper_len ] = range_window[ -upper_len: ]
rc_window[ -lower_len: ] = range_window[ :lower_len ]
# Multiply the simple matched filter by the window
simple_match_filter *= rc_window
# Alternatively, you could load in the matched filter created by the parser
match_filter = simple_match_filter

""" Load in cpi_length pulses of raw data, unattenuate and range compress """
raw_data, pulse_range, attenuation, sys_time = \
    getRawData( debug_dir + '/' + fnme + '_Radar_Data_Channel_1_X-Band.dat',
        cpi_length, 0, isIQ=True )

# Need to unattenuate the data by the attenuation amount applied before the ADC
raw_data *= 10**( attenuation[ None, : ] / 20.0 )

# Range compression
rc_data =\
    np.fft.ifft(
        np.fft.fft( raw_data, fft_len, axis=0 ) * match_filter[ :, None ],
        ifft_len, axis=0 )
# limit the range compression output to only the valid, full convolutions
rc_data = rc_data[ :num_full_conv_bins, : ]

""" Perform the Doppler FFT of range compressed data with a window """
dop_fft_len = cpi_length * dop_interp_factor
dop_window = window_taylor( cpi_length, 4, -70 )
#dop_window = np.ones( cpi_length )
dopp_data = np.fft.fft( rc_data * dop_window[ None, : ], dop_fft_len, axis=1 )
max_dopp_data = db( dopp_data ).max() - 5

""" Plot results """
plt.figure( 'Doppler Range Compressed Data' )
plt.title( 'Pulses {}-{}'.format( 0, cpi_length ) )
plt.imshow(
    np.fft.fftshift( db( dopp_data ), 1 ), cmap='jet', origin='lower',
    vmin=max_dopp_data - 60, vmax=max_dopp_data,
    extent=[ -dopp_prf / 2, dopp_prf / 2, near_range, far_range ] )
plt.ylabel( 'Slant Range (m)' )
plt.xlabel( 'Doppler (Hz)' )
plt.axis( 'tight' )

plt.figure( 'Matched Filter' )
plt.plot( db( match_filter ) )

