import collections
from glob import glob
import numpy as np
from pathlib import Path
from functools import reduce
import mmap
import struct
import pandas as pd
from scipy.interpolate import splev, splrep
from scipy.signal import hilbert, medfilt
import array
from re import search
import pickle
from xml.dom.minidom import parse
from dataclasses import dataclass
from bitarray import bitarray
from bitarray.util import ba2int
from tqdm import tqdm
from typing import Union

# import matplotlib.pyplot as plt

freq_offset = 5e6
DTR = np.pi / 180
mstoknots = 1.94384
c0 = 299792458.0
TAC = 125e6

# The real valued sampling frequency of the DAC
DAC_FREQ_HZ = 4e9
BASE_COMPLEX_SRATE_HZ = DAC_FREQ_HZ / 2
SHRT_MAX = 32767
USHRT_MAX = 65535
INT_MAX = 4294967295
pulseChunkMax = USHRT_MAX * 10000  # No more than 10000 full size pulses per loop
DSCALE = .75 * SHRT_MAX  # * 2e16
# Pan and tilt conversion factors for the gimbal E46_70
GIM_CONV = {'FLIR D48': {'pan': 23.142857 / 3600, 'tilt': 11.571429 / 3600},
            'FLIR E46-70': {'pan': -23.142857 / 3600, 'tilt': 11.571429 / 3600},
            'FLIR D100': {'pan': 27.0 / 3600, 'tilt': 13.5 / 3600},
            'FLIR PTU-5': {'pan': 18.0 / 3600, 'tilt': 18.0 / 3600}}
DATA_START_WORD = b'\x0A\xB0\xFF\x18'
GPS_START_WORD = b'\x0A\xFF\xB0\x18'
GIMBAL_START_WORD = b'\x0A\xFF\x18\xB0'
WAVEFORM_START_WORD = b'\x0A\xB0\xFF\x45'
STOP_WORD = b'\xAA\xFF\x11\x55'
COLLECTION_MODE_OLD_DIGITAL_CHANNEL_MASK = 0x000003E000000000
COLLECTION_MODE_DIGITAL_CHANNEL_MASK = 0x000000F800000000
COLLECTION_MODE_WAVENUMBER_MASK = 0x0000070000000000
COLLECTION_MODE_OLD_WAVENUMBER_MASK = 0x00001C0000000000
COLLECTION_MODE_OPERATION_MASK = 0x1800000000000000
COLLECTION_MODE_DIGITAL_CHANNEL_SHIFT = 35
COLLECTION_MODE_OLD_DIGITAL_CHANNEL_SHIFT = 37
COLLECTION_MODE_WAVENUMBER_SHIFT = 40
COLLECTION_MODE_OLD_WAVENUMBER_SHIFT = 42
COLLECTION_MODE_OPERATION_SHIFT = 59
OVERVOLTAGE_MASK = 0x80

# Used to calculate out mode bit stuff
MODE_SHIFTS = {'digital_channel': [37, 35], 'wavenumber': [42, 40],
               'operation': 59, 'adc_channel': 61, 'receiver_slot': 56, 'receiver_channel': 55, 'upconverter_slot': 52,
               '8/9_select': 51, 'band': [49, 47], 'dac_channel': [46, 44],
               'ddc_enable': [36, 34], 'filter_select': [33, 31],
               'rx_port': [29, 27], 'tx_port': [25, 23], 'polarization': [23, 21],
               'numconsmodes': [20, 18], 'awg_enable': [18, 17],
               'rf_ref_wavenumber': 13}
MODE_SZ = {'digital_channel': [4, 4], 'wavenumber': [3, 3],
           'operation': 2, 'adc_channel': 3, 'receiver_slot': 3, 'receiver_channel': 1, 'upconverter_slot': 3,
           '8/9_select': 1, 'band': [2, 4], 'dac_channel': [3, 3],
           'ddc_enable': [1, 1], 'filter_select': [3, 3],
           'rx_port': [4, 4], 'tx_port': [4, 4], 'polarization': [2, 2],
           'numconsmodes': [3, 3], 'awg_enable': [1, 1],
           'rf_ref_wavenumber': 4}
MODE_BIT_MASK = {'digital_channel': [2061584302080, 515396075520], 'wavenumber': [30786325577728, 7696581394432],
                 'operation': 1729382256910270464, 'adc_channel': 16140901064495857664,
                 'receiver_slot': 504403158265495552, 'receiver_channel': 36028797018963968,
                 'upconverter_slot': 31525197391593472, '8/9_select': 2251799813685248,
                 'band': [1688849860263936, 2111062325329920], 'dac_channel': [492581209243648, 123145302310912],
                 'ddc_enable': [68719476736, 17179869184], 'filter_select': [60129542144, 15032385536],
                 'rx_port': [8053063680, 2013265920], 'tx_port': [503316480, 125829120],
                 'polarization': [25165824, 6291456], 'numconsmodes': [7340032, 1835008],
                 'awg_enable': [262144, 131072], 'rf_ref_wavenumber': 122880}
CAL_SAR_SHIFT = 1024
REC_CHUNK_SZ = 4096
SDR_HEADER_SZ = 28
SAR_HEADER_SZ = 24

MIXDOWN_FREQS = [0, 0, 8e9, 33e9, 4e9, 4e9]
POLARIZATION = ['HH', 'HV', 'VH', 'VV']
OP_MODES = ['Normal', 'Cal', 'Receive_Only', 'RF_Ref']


###############################################################################
# Structures
###############################################################################
# Gimbal data set structure
@dataclass
class GimbalDataSet:
    numFrames: int = 0
    l_pan: np.array = None
    l_tilt: np.array = None
    l_systemTimeTAC: np.array = None


def isSAR(fnme):
    fss = findAllFilenames(fnme)
    xml = loadXMLFile(fss['xml'], True)
    return 'SlimSAR_Configuration' in xml


def readDataHeader(data, dataversion):
    frm = int.from_bytes(data[:4], byteorder='big', signed=False)
    syst = int.from_bytes(data[4:8], byteorder='big', signed=False)
    mode = int.from_bytes(data[8:16], byteorder='big', signed=False)

    # Calc channel number
    if dataversion == 0:
        channel = (mode & COLLECTION_MODE_OLD_DIGITAL_CHANNEL_MASK) >> COLLECTION_MODE_OLD_DIGITAL_CHANNEL_SHIFT
        # Waveform number for this mode
        wave_num = (mode & COLLECTION_MODE_OLD_WAVENUMBER_MASK) >> COLLECTION_MODE_OLD_WAVENUMBER_SHIFT
    else:
        channel = (mode & COLLECTION_MODE_DIGITAL_CHANNEL_MASK) >> COLLECTION_MODE_DIGITAL_CHANNEL_SHIFT
        wave_num = (mode & COLLECTION_MODE_WAVENUMBER_MASK) >> COLLECTION_MODE_WAVENUMBER_SHIFT
    op_mode = (mode & COLLECTION_MODE_OPERATION_MASK) >> COLLECTION_MODE_OPERATION_SHIFT

    nsam = int.from_bytes(data[16:20], byteorder='big', signed=False)
    # Check for overvoltage before masking attenuation
    overvoltage = data[20] & OVERVOLTAGE_MASK
    # Att is only 5 bits so we mask it
    att = data[20] & 0x1f
    # Rest is AGC/reserved data stuff, ignore it
    return frm, syst, op_mode, att, nsam, channel, wave_num, overvoltage


def findall(mm, n):
    ret = [mm.find(n)]
    while ret[-1] != -1:
        mm.seek(ret[-1])
        ret.append(mm.find(n))
    return ret[:-1]


def factors(n):
    return list(set(reduce(list.__add__,
                           ([i, n // i] for i in range(1, int(pow(n, 0.5) + 1)) if n % i == 0))))


def getModeBitsFromHeader(data):
    return int.from_bytes(data[8:16], byteorder='big', signed=False)


def getModeValues(mode, is_old=False):
    # Gets the values of several channel related elements using Mode bits
    rdict = {}
    for key, val in MODE_BIT_MASK.items():
        if isinstance(val, list):
            if is_old:
                rdict[key] = (mode & val[0]) >> MODE_SHIFTS[key][0]
            else:
                rdict[key] = (mode & val[1]) >> MODE_SHIFTS[key][1]
        else:
            rdict[key] = (mode & val) >> MODE_SHIFTS[key]
    return rdict


def recalcModeBitMask():
    mode_bits_mask = {}
    for key, val in MODE_SHIFTS.items():
        mask = bitarray(64)
        mask[:] = 0
        if type(val) == list:
            mask1 = bitarray(64)
            mask1[:] = 0
            mask1[-val[0] - MODE_SZ[key][0]:-val[0]] = 1
            mask2 = bitarray(64)
            mask2[:] = 0
            mask2[-val[1] - MODE_SZ[key][1]:-val[1]] = 1
            mode_bits_mask[key] = [ba2int(mask1), ba2int(mask2)]
        else:
            mask[-val - MODE_SZ[key]:-val] = 1
            mode_bits_mask[key] = ba2int(mask)
    return mode_bits_mask


def getNestedDict(d, key):
    # Find the dict or value in a nested dictionary. Useful for the parsed XML.
    if not isinstance(d, dict):
        return None
    if key in d.keys():
        return d[key]
    ans = None
    for json_key in d.keys():
        r = getNestedDict(d[json_key], key)
        if r is None:
            continue
        else:
            ans = r
    return ans


def parseGimbalDataSet(fid):
    # Create the gimbal data set
    data_set = GimbalDataSet()
    # Read the number of frames
    data_set.numFrames = np.fromfile(fid, count=1, dtype="uint32")[0]
    data_set.l_pan = np.zeros(data_set.numFrames, dtype="int16")
    data_set.l_tilt = np.zeros(data_set.numFrames, dtype="int16")
    data_set.l_systemTimeTAC = np.zeros(data_set.numFrames, dtype="uint32")
    # Read the pan, tilt, and system times
    for i in range(data_set.numFrames):
        # Read the pan positions
        data_set.l_pan[i] = \
            np.fromfile(fid, count=1, dtype="int16")[0]
        # Read the tilt positions
        data_set.l_tilt[i] = \
            np.fromfile(fid, count=1, dtype="int16")[0]
        # Read the system times
        data_set.l_systemTimeTAC[i] = \
            np.fromfile(fid, count=1, dtype="uint32")[0]
    return data_set


def readBESTPOSPacket(data, hl):
    gpsweek = int.from_bytes(data[22:24], byteorder='little')
    gpsms = np.round(int.from_bytes(data[24:28], byteorder='little') / 1000, 2)
    lat = struct.unpack('<d', data[hl + 8:hl + 16])[0]
    lon = struct.unpack('<d', data[hl + 16:hl + 24])[0]
    alt = struct.unpack('<d', data[hl + 24:hl + 32])[0]  # + undulationEGM96(lat, lon)
    undulation = struct.unpack('<f', data[hl + 32:hl + 36])[0]
    return gpsweek, gpsms, lat, lon, alt, undulation


def readINSCOVSPacket(data, hl):
    gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
    gpsms = np.round(struct.unpack('<d', data[hl + 4:hl + 12])[0], 2)
    pos_cov = np.zeros((3, 3))
    att_cov = np.zeros((3, 3))
    vel_cov = np.zeros((3, 3))
    for x in range(3):
        for y in range(3):
            pos_cov[x, y] = \
                struct.unpack('<d', data[hl + 12 + 8 * x + 24 * y:hl + 12 + 8 * x + 24 * y + 8])[0]
    for x in range(3):
        for y in range(3):
            att_cov[x, y] = \
                struct.unpack('<d', data[hl + 84 + 8 * x + 24 * y:hl + 84 + 8 * x + 24 * y + 8])[0]
    for x in range(3):
        for y in range(3):
            vel_cov[x, y] = \
                struct.unpack('<d', data[hl + 156 + 8 * x + 24 * y:hl + 156 + 8 * x + 24 * y + 8])[
                    0]
    return gpsweek, gpsms, pos_cov, att_cov, vel_cov


def readINSPVASPacket(data, hl):
    gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
    gpsms = np.round(struct.unpack('<d', data[hl + 4:hl + 12])[0], 2)
    lat = struct.unpack('<d', data[hl + 12:hl + 20])[0]
    lon = struct.unpack('<d', data[hl + 20:hl + 28])[0]
    alt = struct.unpack('<d', data[hl + 28:hl + 36])[0]
    vel_n = struct.unpack('<d', data[hl + 36:hl + 44])[0]
    vel_e = struct.unpack('<d', data[hl + 44:hl + 52])[0]
    vel_u = struct.unpack('<d', data[hl + 52:hl + 60])[0]
    roll = struct.unpack('<d', data[hl + 60:hl + 68])[0] * DTR
    pitch = struct.unpack('<d', data[hl + 68:hl + 76])[0] * DTR
    yaw = struct.unpack('<d', data[hl + 76:hl + 84])[0] * DTR
    heading = ((360 + np.arctan2(vel_e, vel_n) / DTR) % 360) * DTR
    return gpsweek, gpsms, lat, lon, alt, vel_n, vel_e, vel_u, roll, pitch, yaw, heading


def readTIMESYNCPacket(data, hl):
    gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
    gpsms = struct.unpack('<l', data[hl + 4:hl + 8])[0]
    return gpsweek, gpsms


@dataclass
class Gimbal:
    model: str
    mode: str
    look_side: str
    pan_limit: float
    tilt_limit: float
    dep_angle: float
    squint_angle: float
    update_rate: float
    x_offset: float
    y_offset: float
    z_offset: float
    roll: float
    pitch: float
    yaw: float
    initial_course_angle: float
    scan_rate: float = 0.


class SDRBase(object):
    fnme: str
    files: dict
    xml: dict
    sys_mode: str
    progress_tracker: object
    n_channels: int
    gim: Gimbal
    ant: list
    port: list
    ash: dict
    asi: np.ndarray
    inscovs: pd.DataFrame
    gimbal: pd.DataFrame
    gps_data: pd.DataFrame
    bestpos: pd.DataFrame
    timesync: pd.DataFrame
    dataversion: int
    channels: list

    def __init__(self, fnme: str,
                 files: dict,
                 xml: dict,
                 sys_mode: str,
                 progress_tracker: object,
                 n_channels: int,
                 gim: Union[Gimbal, dict],
                 ant: list,
                 port: list,
                 ash: dict,
                 inscovs: pd.DataFrame,
                 gimbal: pd.DataFrame,
                 gps_data: pd.DataFrame,
                 bestpos: pd.DataFrame,
                 timesync: pd.DataFrame,
                 dataversion: int,
                 channels: list):
        self.fnme = fnme
        self.files = files
        self.xml = xml
        self.sys_mode = sys_mode
        self.progress_tracker = progress_tracker
        self.n_channels = n_channels
        if gim:
            self.gim = gim if isinstance(gim, Gimbal) else Gimbal(**gim)
        self.ant = [Antenna(**a) if isinstance(a, dict) else a for a in ant]
        self.port = [AntennaPort(**p) if isinstance(p, dict) else p for p in port]
        self.ash = ash
        self.inscovs = inscovs
        self.gimbal = gimbal
        self.gps_data = gps_data
        self.bestpos = bestpos
        self.timesync = timesync
        self.dataversion = dataversion
        self.channels = [Channel(**c) if isinstance(c, dict) else c for c in channels]

    def __getitem__(self, it):
        return self.channels[it]

    def __len__(self):
        return len(self.channels)

    def __log__(self, log_item):
        """Logs a string by updating the progress tracker object and printing to the console. If no
        progress tracker object was specified in the constructor, the log item will only be printed
        to the console.

        Args:
            log_item (string): String to be logged
        """
        print(log_item)

    def loadGPS(self, new_gps_data):
        """
        Changes out GPS data.
        :param new_gps_data: DataFrame of GPS data, same format as original.
        :return: Nothing.
        """
        self.gps_data = new_gps_data

    def loadASH(self, fnme=None):
        """
        Loads the .ASH file into the object.
        :param fnme: Path to .ASH file.
        :return: saved .ASH data, as a dict.
        """
        if fnme is not None:
            self.ash = loadASHFile(fnme)
        return self.ash

    def loadASI(self, fnme=None):
        """
        Loads .asi file data, using previously acquired .ash data.
        :param fnme: Path to .asi file.
        :return: Numpy array of complex .asi file data.
        """
        self.asi = loadASIFile(fnme,
                               self.ash['image']['nRows'],
                               self.ash['image']['nCols'])
        return self.asi

    def getBackprojectWavelength(self, channel):
        try:
            bpj_wavelength = c0 / (self[channel].fc - self[channel].bw / 2 - self[channel].xml['DC_Offset_MHz'] * 1e6) \
                if self[channel].xml['Offset_Video_Enabled'].lower() == 'true' else c0 / self[channel].fc
        except KeyError as e:
            f'Could not find {e}'
            bpj_wavelength = c0 / (self[channel].fc - self[channel].bw / 2 - 5e6)
        return bpj_wavelength

    def genInverseTransferFunction(
            self,
            chan_num: int,
            fft_len=None,
            weiner_filter: bool = False) -> np.ndarray:
        """
        Computes and returns the inverse transfer function in the frequency
          domain for the channel with fft_len samples.
        :param chan_num: The channel number
        :param fft_len: The length of the FFT to use. If not provided, the
          default is the next power of two larger than the convolution output
        :param weiner_filter: Flag for whether to create the Weiner filter
          version of the inverse transfer function
        :return: Numpy array with the inverse transfer function
        """
        # Grab the channel for brevity
        chan = self.channels[chan_num]
        # Get the chirp rate in the baseband
        baseband_chirp_rate_hz_per_s = chan.chirp_rate
        # If the NCO was positive it means we will have sampled the reverse
        #   spectrum and the chirp will be flipped
        if chan.NCO_freq_Hz > 0:
            baseband_chirp_rate_hz_per_s *= -1
        half_bandwidth_hz = chan.bw / 2.0
        # Get the baseband center, start and stop frequency of the chirp
        baseband_start_freq_hz = chan.baseband_fc - half_bandwidth_hz
        baseband_stop_freq_hz = chan.baseband_fc + half_bandwidth_hz
        if baseband_chirp_rate_hz_per_s < 0:
            baseband_start_freq_hz = chan.baseband_fc + half_bandwidth_hz
            baseband_stop_freq_hz = chan.baseband_fc - half_bandwidth_hz

        # Get the reference waveform and mix it down by the NCO frequency and
        #   down-sample to the sampling rate of the receive data if necessary
        # The waveform input into the DAC has already had the Hilbert transform
        #   and down-sample operation performed on it by SDRParsing, so it is
        #   complex sampled data at the SlimSDR base complex sampling rate by
        #   this point.
        # Compute the decimation rate if the data has been low-pass filtered and
        #   down-sampled
        decimation_rate = 1
        if chan.is_lpf:
            decimation_rate = int(
                np.floor(BASE_COMPLEX_SRATE_HZ / chan.fs))

        # Get the length of the waveform
        waveform_len = len(chan.ref_chirp)
        # Compute the mixdown signal
        mix_down_signal = np.exp(
            1j * (2 * np.pi * chan.NCO_freq_Hz * np.arange(waveform_len)
                  / BASE_COMPLEX_SRATE_HZ))
        baseband_waveform = mix_down_signal * chan.ref_chirp

        # Decimate the waveform if applicable
        if decimation_rate > 1:
            baseband_waveform = baseband_waveform[:: decimation_rate]
        # Calculate the updated baseband waveform length
        baseband_waveform_len = len(baseband_waveform)

        # Calculate the convolution length
        convolution_length = chan.nsam + baseband_waveform_len - 1
        fft_length =\
            findPowerOf2(convolution_length) if fft_len is None else fft_len

        # Pre-compute the FFT of the baseband waveform and the cal data
        fft_baseband_waveform = np.fft.fft(baseband_waveform, fft_length)
        fft_cal_chirp = np.fft.fft(chan.cal_chirp, fft_length)
        # Compute the inverse transfer function
        inverse_transfer_function = None
        if weiner_filter:
            jazz = fft_cal_chirp * fft_baseband_waveform.conj()
            epsilon = abs(jazz).min() * 1e4
            inverse_transfer_function =\
                (fft_baseband_waveform * fft_baseband_waveform.conj()
                 / (fft_cal_chirp * fft_baseband_waveform.conj() + epsilon))
        else:
            inverse_transfer_function = fft_baseband_waveform / fft_cal_chirp
            # NOTE! Outside the bandwidth of the signal, the inverse transfer
            #   function is invalid and should not be viewed. Values will be
            #   enormous.

            # Compute number of frequency samples for the bandwidth
            bw_size = int(np.floor(
                half_bandwidth_hz * 2.0 / chan.fs * fft_length))

            # Polish up the inverse transfer function
            # IQ baseband vs offset video
            if (np.sign(baseband_start_freq_hz)
                    != np.sign(baseband_stop_freq_hz)):
                # Apply the inverse transfer function
                above_zero_length = int(np.ceil(
                    (chan.baseband_fc + half_bandwidth_hz)
                    / chan.fs * fft_length))
                below_zero_length = int(bw_size - above_zero_length)
                # Zero out the invalid part of the inverse transfer function
                inverse_transfer_function[
                    above_zero_length: -below_zero_length] = 0

            else:
                # Apply the inverse transfer function
                band_start_ind = int(np.floor(
                    (chan.baseband_fc - half_bandwidth_hz)
                    / chan.fs * fft_length))
                inverse_transfer_function[: band_start_ind] = 0
                inverse_transfer_function[band_start_ind + bw_size:] = 0
        return inverse_transfer_function

    def genMatchedFilter(
            self,
            chan_num: int,
            nbar: int = 5,
            SLL: int = -35,
            fft_len: int = None) -> np.ndarray:
        """
        
        Args:
            chan_num: 
            nbar: 
            SLL: 
            fft_len: 

        Returns:

        """
        chan = self.channels[chan_num]
        # Things the PS will need to know from the configuration
        numSamples = chan.nsam
        samplingFreqHz = chan.fs
        basebandedChirpRateHzPerS = chan.chirp_rate
        # If the NCO was positive it means we will have sampled the reverse spectrum
        #   and the chirp will be flipped
        if chan.NCO_freq_Hz > 0:
            basebandedChirpRateHzPerS *= -1
        halfBandwidthHz = chan.bw / 2.0
        # Get the basebanded center, start and stop frequency of the chirp
        basebandedCenterFreqHz = chan.baseband_fc
        basebandedStartFreqHz = chan.baseband_fc - halfBandwidthHz
        basebandedStopFreqHz = chan.baseband_fc + halfBandwidthHz
        if basebandedChirpRateHzPerS < 0:
            basebandedStartFreqHz = chan.baseband_fc + halfBandwidthHz
            basebandedStopFreqHz = chan.baseband_fc - halfBandwidthHz

        # Get the reference waveform and mix it down by the NCO frequency and
        #   downsample to the sampling rate of the receive data if necessary
        # The waveform input into the DAC has already had the Hilbert transform
        #   and downsample operation performed on it by SDRParsing, so it is
        #   complex sampled data at this point at the SlimSDR base complex sampling
        #   rate.
        # Compute the decimation rate if the data has been low-pass filtered and
        #   downsampled
        decimationRate = 1
        if chan.is_lpf:
            decimationRate = int(np.floor(BASE_COMPLEX_SRATE_HZ / samplingFreqHz))

        # Grab the waveform
        waveformData = chan.ref_chirp

        # Create the plot for the FFT of the waveform
        waveformLen = len(waveformData)

        # Compute the mixdown signal
        mixDown = np.exp(1j * (2 * np.pi * chan.NCO_freq_Hz * np.arange(waveformLen) / BASE_COMPLEX_SRATE_HZ))
        basebandWaveform = mixDown * waveformData

        # Decimate the waveform if applicable
        if decimationRate > 1:
            basebandWaveform = basebandWaveform[:: decimationRate]
        # Calculate the updated baseband waveform length
        basebandWaveformLen = len(basebandWaveform)
        # Grab the calibration data
        calData = chan.cal_chirp + 0.0

        # Calculate the convolution length
        convolutionLength = numSamples + basebandWaveformLen - 1
        FFTLength = findPowerOf2(convolutionLength) if fft_len is None else fft_len

        # Calculate the inverse transfer function
        FFTCalData = np.fft.fft(calData, FFTLength)
        FFTBasebandWaveformData = np.fft.fft(basebandWaveform, FFTLength)
        inverseTransferFunction = FFTBasebandWaveformData / FFTCalData
        # NOTE! Outside of the bandwidth of the signal, the inverse transfer function
        #   is invalid and should not be viewed. Values will be enormous.

        # Generate the Taylor window
        TAYLOR_NBAR = nbar
        TAYLOR_SLL_DB = SLL
        windowSize = \
            int(np.floor(halfBandwidthHz * 2.0 / samplingFreqHz * FFTLength))
        taylorWindow = window_taylor(windowSize, nbar=TAYLOR_NBAR, sll=TAYLOR_SLL_DB) if SLL != 0 else np.ones(
            windowSize)

        # Create the matched filter and polish up the inverse transfer function
        matchedFilter = np.fft.fft(basebandWaveform, FFTLength)
        # IQ baseband vs offset video
        if np.sign(basebandedStartFreqHz) != np.sign(basebandedStopFreqHz):
            # Apply the inverse transfer function
            aboveZeroLength = int(np.ceil((basebandedCenterFreqHz + halfBandwidthHz) / samplingFreqHz * FFTLength))
            belowZeroLength = int(windowSize - aboveZeroLength)
            taylorWindowExtended = np.zeros(FFTLength)
            taylorWindowExtended[
            int(FFTLength / 2) - aboveZeroLength:int(FFTLength / 2) - aboveZeroLength + windowSize] = \
                taylorWindow
            # Zero out the invalid part of the inverse transfer function
            inverseTransferFunction[aboveZeroLength: -belowZeroLength] = 0
            taylorWindowExtended = np.fft.fftshift(taylorWindowExtended)
        else:
            # Apply the inverse transfer function
            bandStartInd = \
                int(np.floor((basebandedCenterFreqHz - halfBandwidthHz) / samplingFreqHz * FFTLength))
            taylorWindowExtended = np.zeros(FFTLength)
            taylorWindowExtended[bandStartInd: bandStartInd + windowSize] = taylorWindow
            inverseTransferFunction[: bandStartInd] = 0
            inverseTransferFunction[bandStartInd + windowSize:] = 0
        matchedFilter = matchedFilter.conj() * inverseTransferFunction * taylorWindowExtended
        return matchedFilter

    def getPulse(self, pulse_num, channel=0, is_cal=False):
        return self.getPulses([pulse_num], channel, is_cal)

    def getPulses(self, pulse_nums, channel=0, is_cal=False):
        """
        Gets pulse data from file.
        :param pulse_nums: Number of pulses wanted.
        :param channel: int Channel number of pulses wanted.
        :param is_cal: If True, gets the calibration pulse of number {pulse_num}. Otherwise, gets the
            data pulse.
        :return: pulse data as a complex numpy array.
        """
        chan = self.channels[channel]
        ref_num = chan.cal_num if is_cal else chan.frame_num
        ref_pts = chan.cal_pts if is_cal else chan.packet_points
        pn = np.arange(len(ref_num), dtype=int)
        _frames = [pn[ref_num == n][0] for n in pulse_nums if n in ref_num]
        if _exc := [n for n in pulse_nums if n not in ref_num]:
            raise RuntimeWarning(f'Pulses {_exc} not found in pulse listing. Perhaps they were overvoltaged?')
        pul_data = np.zeros((chan.nsam, len(_frames)), dtype=np.complex128)
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            for idx, p in enumerate(_frames):
                try:
                    pp = ref_pts[p]
                    atts = 31 if is_cal else chan.atts[p]
                except IndexError:
                    self.__log__(f'IDX is {idx} and p is {p}')
                mm.seek(pp)
                data = mm.read(4 * chan.nsam)
                tmp_data = array.array('h', data)
                tmp_data.byteswap()
                tmp_data = np.array(tmp_data)
                pul_data[:, idx] = (tmp_data[:chan.nsam * 2:2] + 1j * tmp_data[1:chan.nsam * 2:2]) * 10 ** (atts / 20)
        return chan.cal_time[_frames] if is_cal else chan.sys_time[_frames], pul_data

    def getPulseGen(self, chunk_sz=64, channel=0, is_cal=False):
        """
        This is a generator function version of getPulse, above.
        :param channel: int Number of channel to get iterator for.
        :param chunk_sz: Size of each chunk returned by the generator.
        :param is_cal: If True, gets cal data pulse.
        :return: Generator with
            pdata - pulse data as a numpy array.
            pulse_nums - list of pulse numbers.
            atts - attenuation factors of pulses returned.
            sys_time - system times, in TAC, of pulses returned.
        """
        chan = self.channels[channel]
        ref_pts = chan.cal_pts if is_cal else chan.packet_points

        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            for idx in range(0, len(ref_pts), chunk_sz):
                chunk_pts = ref_pts[idx:min(len(ref_pts), idx + chunk_sz)]
                pul_data = np.zeros((chan.nsam, len(chunk_pts)), dtype=np.complex128)
                for i, p in enumerate(chunk_pts):
                    try:
                        pp = ref_pts[p]
                        atts = 31 if is_cal else chan.atts[p]
                    except IndexError:
                        self.__log__(f'IDX is {i} and p is {p}')
                    mm.seek(pp)
                    data = mm.read(4 * chan.nsam)
                    tmp_data = array.array('h', data)
                    tmp_data.byteswap()
                    tmp_data = np.array(tmp_data)
                    pul_data[:, i] = (tmp_data[:chan.nsam * 2:2] + 1j * tmp_data[1:chan.nsam * 2:2]) * 10 ** (atts / 20)
                    yield pul_data, chunk_pts, chan.atts[chunk_pts], chan.sys_time[chunk_pts]

    def dump(self):
        pdict = self.__dict__.copy()
        pdict['gim'] = self.gim.__dict__.copy()
        pdict['ant'] = [a.__dict__.copy() for a in self.ant]
        pdict['port'] = [p.__dict__.copy() if isinstance(p, AntennaPort) else [] for p in self.port]
        pdict['channels'] = [c.__dict__.copy() if isinstance(c, Channel) else None for c in self.channels]
        pdict['progress_tracker'] = None
        return pdict


class SDRParse(SDRBase):

    def __init__(self, fnme: str = None, nframes: int = None, progress_tracker: bool = None, skip_gps: bool = False,
                 use_inscovs: bool = False, do_exact_matches: bool = False, use_jump_correction: bool = False,
                 pol: str = None):
        # Loop through filename directory and find files associated with .sar file
        if '.sar' in fnme:
            files = findAllFilenames(fnme, exact_matches=do_exact_matches)
        else:
            # Assume the fnme given is the base name for all the files
            files = {'xml': fnme + '.xml', 'asi': fnme + '.asi'}
            fnme = fnme + '.sar'
        full_xml = loadXMLFile(files['xml'], True)['SlimSDR_Configuration']
        xml = full_xml['SlimSDR_Info']
        try:
            sys_mode = xml['System_Mode']
        except KeyError:
            sys_mode = 'SAR'
        is_df = (sys_mode == 'DF')
        # Progress tracker object with append_log function
        if progress_tracker is True:
            progress_tracker = tqdm(desc='Progress')
        ants = []
        ports = []
        waveforms = {}
        raw_waveforms = {}
        # Find the Gimbal Settings
        gim = None
        try:
            g = getNestedDict(full_xml, 'Gimbal_Settings')
            if g is not None:
                scan_rate = g['GMTI_Scanning_Settings']['Scan_Rate_Degrees_Per_Second'] \
                    if 'GMTI_Scanning_Settings' in g else 0
                gdepang = g['Gimbal_Depression_Angle_D'] \
                    if 'Gimbal_Depression_Angle_D' in g else g['Depression_Angle_D']
                ginit_ang = g['Initial_Course_Angle_R'] if 'Initial_Course_Angle_R' in g else g['Initial_Course_Angle_D'] * DTR
                gim = Gimbal(g['Gimbal_Model'], g['Stabilization_Mode'], g['Gimbal_Look_Side'],
                             g['Pan_Limits_D'], g['Tilt_Limits_D'], gdepang,
                             g['Squint_Angle_D'], g['Update_Rate_Hz'], g['Gimbal_X_Offset_M'],
                             g['Gimbal_Y_Offset_M'], g['Gimbal_Z_Offset_M'], g['Roll_D'], g['Pitch_D'],
                             g['Yaw_D'], ginit_ang, scan_rate)
        except KeyError as e:
            print(f'Error loading gimbal: {e}')
        # Find the Antennas
        ant_num = 0
        antenna = getNestedDict(xml, f'Antenna_{ant_num}')
        while antenna is not None:
            ax_offset = antenna['Antenna_X_Offset_M'] if 'Antenna_X_Offset_M' in antenna else None
            ay_offset = antenna['Antenna_Y_Offset_M'] if 'Antenna_Y_Offset_M' in antenna else None
            az_offset = antenna['Antenna_Z_Offset_M'] if 'Antenna_Z_Offset_M' in antenna else None
            aabw = antenna['Azimuth_Beamwidth_D'] * DTR if 'Azimuth_Beamwidth_D' in antenna else None
            aebw = antenna['Elevation_Beamwidth_D'] * DTR if 'Elevation_Beamwidth_D' in antenna else None
            adbw = antenna['Doppler_Beamwidth_D'] * DTR if 'Doppler_Beamwidth_D' in antenna else None
            ada = antenna['Antenna_Depression_Angle_D'] * DTR if 'Antenna_Depression_Angle_D' in antenna else None
            anapc = antenna['Num_Azimuth_Phase_Centers'] if 'Num_Azimuth_Phase_Centers' in antenna else None
            anepc = antenna['Num_Elevation_Phase_Centers'] if 'Num_Elevation_Phase_Centers' in antenna else None
            asa = antenna['Antenna_Squint_Angle_D'] * DTR if 'Antenna_Squint_Angle_D' in antenna else None
            isw = antenna['Is_Split_Weight_Antenna'] == 'true' if 'Is_Split_Weight_Antenna' in antenna else False
            if ax_offset is None:
                ants.append(Antenna(aabw, aebw, adbw, ada, anapc, anepc, asa, isw))
            else:
                ants.append(Antenna(aabw, aebw, adbw, ada, anapc, anepc, asa, isw))
                ports.append(AntennaPort(ax_offset, ay_offset,
                                         az_offset, len(ants) - 1, len(ants) - 1))
            ant_num += 1
            antenna = getNestedDict(xml, f'Antenna_{ant_num}')

        if progress_tracker is not None:
            progress_tracker.set_description('Loaded antennas')

        # Find the Antenna Ports
        try:
            ant_ports_per_ant = [np.arange(ant.az_phase_centers * ant.el_phase_centers) for ant in ants]
            ant_ports_per_ant = [ant_ports_per_ant[n] + np.sum([len(a) for a in ant_ports_per_ant][:n]) for n in
                                 range(len(ants))]
            for port_num in range(10):
                a_port = getNestedDict(xml, f'Antenna_Port_{port_num}')
                if a_port is not None:
                    curr_ant = [n for n in range(len(ants)) if port_num in ant_ports_per_ant[n]][0]
                    ports.append(AntennaPort(a_port['Port_X_Offset_M'], a_port['Port_Y_Offset_M'],
                                             a_port['Port_Z_Offset_M'], port_num, curr_ant))
                else:
                    ports.append([])
        except TypeError:
            print('No antenna ports found in XML.')

        if progress_tracker is not None:
            progress_tracker.set_description('Loaded antenna ports')

        # There is some extra data in the .ash file that may be wanted
        # It's not necessary, though, so just check for it
        ash = None
        if 'ash' in files:
            if isinstance(files['ash'], list):
                ash = loadASHFile(files['ash'][0])
            elif isinstance(files['ash'], dict):
                ash = loadASHFile(files['ash'][pol])
            elif isinstance(files['ash'], str):
                ash = loadASHFile(files['ash'])
            else:
                self.__log__('No ASH file found')

        if progress_tracker is not None:
            progress_tracker.set_description('Loaded ASH file')

        timesync_n = 0

        # These are Pandas Dataframes of the various ancillary things to the radar, such as GPS and Gimbal data
        gps_df = pd.DataFrame(columns=['lat', 'lon', 'alt',
                                       'vn', 've', 'vu', 'r', 'p', 'y', 'heading', 'systime',
                                       'corr_systime', 'gps_wk', 'ptype'], dtype=np.float64)
        bestpos = pd.DataFrame(columns=['lat', 'lon', 'alt',
                                        'vn', 've', 'vu', 'r', 'p', 'y', 'heading', 'systime',
                                        'corr_systime', 'gps_wk', 'undulation', 'ptype'], dtype=np.float64)
        timesync = pd.DataFrame(columns=['week', 'secs', 'systime', 'corr_systime'], dtype=np.float64)
        gimbal_df = pd.DataFrame(columns=['pan', 'tilt', 'systime', 'corr_systime'], dtype=np.float64)
        inscovs = pd.DataFrame(columns=['Pos', 'Att', 'Vel', 'gpsweek'], dtype=np.float64) if use_inscovs else None
        gimbalDataSet = None

        # Open the .sar file and read contents
        with open(fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)

            # Determine dataversion from first bytes in file
            data_recorder_bytes = mm.read(4)
            if data_recorder_bytes == bytes([0x00, 0xA0, 0x57, 0x27]):
                mm.read(1020)
                dataversion = int.from_bytes(mm.read(1), byteorder='little')
            else:
                mm.seek(0)
                dataversion = int.from_bytes(mm.read(1), byteorder='little')

            # Get channel specific data for each channel now we have dataversion
            # SlimSDR has 8 possible channels, so make a list that long
            channels: list[[None, Channel]] = [None for _ in range(8)]
            ch_num = 0
            ch = getNestedDict(xml, f'Channel_{ch_num}')
            while ch is not None:
                if 'Mode' in ch:
                    if isinstance(ch['Mode'], float):
                        mode = int(f'{ch["Mode"]:.0f}', 16)
                    else:
                        mode = int(ch['Mode'], 16)
                    mvalues = getModeValues(mode, not dataversion)
                    # Just slap another channel in here
                    channels[mvalues['digital_channel']] = Channel(ch, mode, not dataversion)
                else:
                    channels[ch_num] = Channel(ch, is_old=not dataversion)
                if 'Simultaneous_Receive_Modes' in ch:
                    try:
                        num_sim_modes = ch['Simultaneous_Receive_Modes'].split(' ')[:-1]
                    except AttributeError:
                        num_sim_modes = [str(int(ch['Simultaneous_Receive_Modes']))]
                    for mode in num_sim_modes:
                        if isinstance(mode, str):
                            mode = int(mode, 16)
                        mvalues = getModeValues(mode, not dataversion)
                        # Just slap another channel in here
                        channels[mvalues['digital_channel']] = Channel(ch, mode, not dataversion)
                ch_num += 1
                ch = getNestedDict(xml, f'Channel_{ch_num}')
            if is_df:
                # DF has a slightly different approach to channels, so we'll just add all the XML info to it
                ch_xml = {**full_xml, **xml}
                base_channel = Channel(ch_xml, is_old=not dataversion)
                channels[base_channel.mode_settings['digital_channel']] = base_channel
                if 'Simultaneous_Receive_Modes' in xml:
                    try:
                        num_sim_modes = xml['Simultaneous_Receive_Modes'].split(' ')[:-1]
                    except AttributeError:
                        num_sim_modes = [str(int(xml['Simultaneous_Receive_Modes']))]
                    for mode in num_sim_modes:
                        if isinstance(mode, str):
                            mode = int(mode, 16)
                        mvalues = getModeValues(mode, not dataversion)
                        channels[mvalues['digital_channel']] = Channel(ch_xml, mode, not dataversion)
            self.n_channels = sum([c is not None for c in channels])
            if progress_tracker is not None:
                progress_tracker.set_description('Loaded channel data')

            # Little memory optimization to set num_frames
            mm.seek(mm.rfind(DATA_START_WORD) + 4)
            data = mm.read(SDR_HEADER_SZ)
            max_frame, _, _, _, _, _, _, _ = readDataHeader(data, dataversion)
            if progress_tracker is not None:
                progress_tracker.set_description('Parsing file')
                progress_tracker.reset(total=mm.size() // 1024)
            ch_len = max_frame + 1
            for ch in channels:
                if ch:
                    ch.data_pts = np.ones((ch_len,)) * -1
                    ch.frame_num = np.ones((ch_len,)) * -1
                    ch.sys_time = np.ones((ch_len,)) * -1
                    ch.cals = np.ones((ch_len,)) * -1
                    ch.atts = np.ones((ch_len,)) * -1
            mm.seek(0)
            # Get all data start points in a convenient list
            if skip_gps:
                start_words = [DATA_START_WORD, -1, -1, WAVEFORM_START_WORD]
                dsp = [mm.find(start_words[0]), -1, -1, mm.find(start_words[3])]
            else:
                start_words = [DATA_START_WORD, GPS_START_WORD, GIMBAL_START_WORD, WAVEFORM_START_WORD]
                # If the start word is not in the first 100 MB, it's probably not in the file
                dsp = [mm.find(nn, 0, 100000000) for nn in start_words]
            dsp = [d if d is not None else -1 for d in dsp]
            # Is final start point?
            dfp = [False if dsp[nn] == -1 else True for nn in range(len(dsp))]
            dsp = [np.inf if nn == -1 else nn for nn in dsp]
            last_pos = 0
            channels[0].nsam = 0
            while np.any(dfp):
                next_packet = np.argmin(dsp)
                '''if mm.tell() + 4 + SDR_HEADER_SZ + channels[0].nsam * 4 > mm.size():
                    self.__log__('Seeking out of file.')
                    break'''
                if next_packet == 0:
                    mm.seek(dsp[next_packet])
                    poss_pt = mm.tell()

                    mm.read(4)  # Skip start word
                    data = mm.read(SDR_HEADER_SZ)
                    frm, syst, op_mode, att, tmp_nsam, channel, chan_wavenum, overvoltage = \
                        readDataHeader(data, dataversion)
                    try:
                        if channels[channel].nsam == 0:
                            channels[channel].nsam = tmp_nsam
                        nsam = tmp_nsam
                        if tmp_nsam != channels[channel].nsam:
                            self.__log__('Found a different nsam. skipping.')
                            mm.seek(poss_pt + 4 + SDR_HEADER_SZ)
                    except IndexError:
                        self.__log__(f'Incompatible channel found: {channel}. skipping.')
                        mm.seek(poss_pt + 4 + SDR_HEADER_SZ)

                    if poss_pt + 4 + SDR_HEADER_SZ + nsam * 4 > mm.size():
                        self.__log__('Seeking out of file.')
                        break
                    else:
                        mm.seek(nsam * 4, 1)
                    if mm.read(4) == STOP_WORD:
                        # Ignore pulses with overvoltage
                        if not overvoltage:
                            try:
                                if op_mode == 3:
                                    # This is reference chirp mode
                                    mm.seek(dsp[next_packet] + 4 + SDR_HEADER_SZ)
                                    data = mm.read(4 * nsam)
                                    tmp_data = array.array('h', data)
                                    tmp_data.byteswap()
                                    tmp_data = np.array(tmp_data)
                                    ndata = (tmp_data[0:nsam * 2:2] + 1j * tmp_data[1:nsam * 2:2]) * (
                                            10 ** (att / 20))
                                    channels[channel].rf_ref = ndata
                                    mm.read(4)
                                else:
                                    if frm >= len(channels[channel].data_pts):
                                        es = np.ones((frm - len(channels[channel].data_pts) + 1,)) * -1
                                        channels[channel].data_pts = np.concatenate((channels[channel].data_pts, es))
                                        channels[channel].frame_num = np.concatenate((channels[channel].frame_num, es))
                                        channels[channel].sys_time = np.concatenate((channels[channel].sys_time, es))
                                        channels[channel].cals = np.concatenate((channels[channel].cals, es))
                                        channels[channel].atts = np.concatenate((channels[channel].atts, es))
                                    channels[channel].data_pts[frm] = poss_pt + 4 + SDR_HEADER_SZ
                                    channels[channel].frame_num[frm] = frm
                                    channels[channel].sys_time[frm] = syst
                                    channels[channel].cals[frm] = op_mode == 1
                                    channels[channel].atts[frm] = att
                                    channels[channel].nsam = nsam
                                    channels[channel].wavenum = chan_wavenum
                            except IndexError:
                                self.__log__(f'Channel {channel} not found.')
                        else:
                            pass
                            # self.__log__(f'Overvoltage of channel {channel} at pulse {frm}')
                        if nframes is not None:
                            em_stop = True
                            for chan in channels:
                                if len(chan.frame_num) == 0:
                                    em_stop = False
                                elif chan.frame_num[-1] < nframes:
                                    em_stop = False
                            if em_stop:
                                break
                    else:
                        if mm.tell() + nsam * 4 > mm.size():
                            self.__log__('Seeking out of file.')
                            break
                        else:
                            mm.seek(poss_pt + 4 + SDR_HEADER_SZ)
                elif next_packet == 1:
                    # GPS Packet
                    mm.seek(dsp[next_packet])
                    data = mm.read(15)
                    packet_type = int.from_bytes(data[12:14], byteorder='little')
                    header_length = data[11]
                    ppstime = int.from_bytes(data[4:8], signed=False, byteorder='big')
                    hl = header_length + 8
                    if packet_type == 42:
                        # BESTPOS packet
                        data += mm.read(hl + 36 - 15)
                        gpsweek, gpsms, lat, lon, alt, undulation = readBESTPOSPacket(data, hl)
                        bestpos.loc[gpsms,
                        ['lat', 'lon', 'alt', 'systime', 'gps_wk', 'undulation', 'ptype']] = \
                            [lat, lon, alt, ppstime, gpsweek, undulation, 0]
                    elif packet_type == 508:
                        hl = 8 + 12
                        # INSPVAS packet
                        data += mm.read(hl + 84 - 15)
                        gpsweek, gpsms, lat, lon, alt, vel_n, vel_e, vel_u, roll, pitch, yaw, heading = \
                            readINSPVASPacket(data, hl)
                        gps_df.loc[gpsms, ['lat', 'lon', 'alt',
                                           'vn', 've', 'vu', 'r', 'p', 'y', 'heading', 'systime',
                                           'gps_wk', 'ptype']] = \
                            [lat, lon, alt, vel_n, vel_e, vel_u, roll,
                             pitch, yaw, heading, ppstime, gpsweek, 1]
                    elif packet_type == 320 and use_inscovs:
                        # INSCOVS
                        hl = 8 + 12
                        data += mm.read(hl + 228 - 15)
                        gpsweek, gpsms, pos_cov, att_cov, vel_cov = readINSCOVSPacket(data, hl)
                        inscovs.loc[gpsms, ['Pos', 'Att', 'Vel', 'gpsweek']] = \
                            np.array([pos_cov, att_cov, vel_cov, gpsweek], dtype=object)
                    elif packet_type == 492:
                        # TIMESYNC
                        data += mm.read(hl + 8 - 15)
                        gpsweek, gpsms = readTIMESYNCPacket(data, hl)
                        timesync.loc[timesync_n, ['week', 'secs', 'systime']] = \
                            [gpsweek, np.round(gpsms / 1000, 2), ppstime]
                        timesync_n += 1
                    mm.seek(dsp[next_packet] + 4)
                elif next_packet == 2:
                    # Gimbal packet
                    mm.seek(dsp[next_packet])
                    if gimbalDataSet is None:
                        gimbalDataSet = GimbalDataSet()
                    data = mm.read(16)
                    syst = int.from_bytes(data[4:8], byteorder='big')
                    pan = int.from_bytes(data[8:10], byteorder='big', signed=True) \
                          * GIM_CONV[gim.model]['pan'] * DTR
                    tilt = int.from_bytes(data[10:12], byteorder='big', signed=True) \
                           * GIM_CONV[gim.model]['tilt'] * DTR
                    if gimbalDataSet.l_pan is not None:
                        gimbalDataSet.l_pan = np.concatenate((gimbalDataSet.l_pan, [pan]))
                    else:
                        gimbalDataSet.l_pan = [pan]
                    if gimbalDataSet.l_tilt is not None:
                        gimbalDataSet.l_tilt = np.concatenate((gimbalDataSet.l_tilt, [tilt]))
                    else:
                        gimbalDataSet.l_tilt = [tilt]
                    if gimbalDataSet.l_systemTimeTAC is not None:
                        gimbalDataSet.l_systemTimeTAC = np.concatenate((gimbalDataSet.l_systemTimeTAC, [syst]))
                    else:
                        gimbalDataSet.l_systemTimeTAC = [syst]
                    gimbal_df.loc[len(gimbal_df.index)] = [pan, tilt, syst, syst]
                elif next_packet == 3:
                    mm.seek(dsp[next_packet] + 4)
                    data = mm.read(5)
                    # Waveform packet
                    waveform_num = data[0]
                    num_samples = int.from_bytes(data[1:], byteorder='big')
                    data = mm.read(num_samples * 2)
                    stop_word = mm.read(4)
                    # Read the waveform data
                    if stop_word == STOP_WORD:
                        if len(data) % 2 == 0:
                            waveform = np.array(array.array('h', data))
                            waveforms[waveform_num] = hilbert(waveform)[::2]
                            raw_waveforms[waveform_num] = waveform
                    else:
                        mm.seek(dsp[next_packet] + 4)
                dsp[next_packet] = mm.find(start_words[next_packet], mm.tell(), mm.tell() + 10000000)
                dfp[next_packet] = False if dsp[next_packet] == -1 else True
                dsp[next_packet] = dsp[next_packet] if dfp[next_packet] else np.inf
                if progress_tracker is not None:
                    progress_tracker.update((mm.tell() - last_pos) // 1024)
                    last_pos = mm.tell()

            # Parse out gimbal stuff
            if gimbalDataSet is not None:
                for n in range(gimbalDataSet.numFrames):
                    gimbal_df.loc[len(gimbal_df.index)] = [
                        gimbalDataSet.l_pan[n] * GIM_CONV[gim.model]['pan'] * DTR,
                        gimbalDataSet.l_tilt[n] * GIM_CONV[gim.model]['tilt'] * DTR,
                        gimbalDataSet.l_systemTimeTAC[n],
                        gimbalDataSet.l_systemTimeTAC[n]]

        # Final GPS frame calculations
        gps_df['frames'] = gps_df.shape[0]

        # INSPVA systime jump corrections
        jumps = 0
        prev = 0
        for idx, row in gps_df.iterrows():
            if row['systime'] < prev:
                jumps += 1
            prev = row['systime']
            gps_df.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
        gps_df['systime'] = gps_df['corr_systime']
        gps_df = gps_df.drop(columns='corr_systime').astype(np.float64)
        gps_df['frames'] = gps_df.shape[0]
        if progress_tracker is not None:
            progress_tracker.set_description('Jump corrections complete')

        # Jump correction for TIMESYNC systime
        jumps = 0
        prev = 0
        for idx, row in timesync.iterrows():
            if row['systime'] < prev:
                jumps += 1
            prev = row['systime']
            timesync.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
        timesync['systime'] = timesync['corr_systime']
        timesync = timesync.drop(columns='corr_systime').astype(np.float64)

        # Interpolate to correct systime points
        gps_df = gps_df.sort_index()
        try:
            if timesync.shape[0] > 0:
                tac_per_sec = np.diff(timesync['systime'].values).mean()
                timesync_secs = np.concatenate(([timesync.loc[0, 'secs'] - 1], timesync['secs'].values,
                                                [timesync.loc[timesync.shape[0] - 1, 'secs'] + 1]))
                timesync_tac = np.concatenate(([timesync.loc[0, 'systime'] - tac_per_sec], timesync['systime'].values,
                                               [timesync.loc[timesync.shape[0] - 1, 'systime'] + tac_per_sec]))
                gps_df['systime'] = np.interp(gps_df.index.values, timesync_secs, timesync_tac)
            if gps_df.shape[0] > 0 and use_jump_correction:
                # The BESTPOS packet uses WGS84 without the undulation, we gotta add it in
                # gps_df = gps_df.loc[gps_df['ptype'] == 1]
                # gps_df = gps_df.sort_index().dropna()
                gps_df['lat'] = jumpCorrection(gps_df['lat'].values)
                gps_df['lon'] = jumpCorrection(gps_df['lon'].values)
                gps_df['alt'] = jumpCorrection(gps_df['alt'].values)
                # gps_df = gps_df.interpolate(method='index')
        except KeyError:
            self.__log__('No GPS data found.')

        if progress_tracker is not None:
            progress_tracker.set_description('GPS interpolation complete')

        # Final gimbal frame calcs
        if gimbal_df.shape[0] == 0:
            gimbal_df = None
            self.__log__('No gimbal data found')
        else:
            # Gimbal systime jump corrections
            jumps = 0
            prev = 0
            for idx, row in gimbal_df.iterrows():
                if row['systime'] < prev:
                    jumps += 1
                prev = row['systime']
                gimbal_df.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
            gimbal_df['systime'] = gimbal_df['corr_systime']
            gimbal_df = gimbal_df.drop(columns='corr_systime').astype(np.float64)
            gimbal_df = gimbal_df.sort_values(by=['systime'])

        if progress_tracker is not None:
            progress_tracker.set_description('Gimbal data complete')

        # Final INSCOVS frame calcs
        if use_inscovs and inscovs.shape[0] == 0:
            self.__log__('No INSCOVS data found')

        for chan in channels:
            if chan is None:
                continue

            # Remove any extra data space
            chan.data_pts = chan.data_pts[chan.data_pts != -1].astype(np.int64)
            chan.frame_num = chan.frame_num[chan.frame_num != -1].astype(int)
            chan.sys_time = chan.sys_time[chan.sys_time != -1]
            chan.cals = chan.cals[chan.cals != -1].astype(bool)
            chan.atts = chan.atts[chan.atts != -1]
            chan.unwrapSystemTime()

            if len(chan.data_pts) == 0:
                raise RuntimeWarning('Channel has no data.')
            else:
                # Remove cal data
                chan.ncals = sum(chan.cals)
                if not chan.is_receive_only:
                    chan.cal_num = chan.frame_num[chan.cals]
                    chan.cal_pts = chan.data_pts[chan.cals]
                    chan.cal_time = chan.sys_time[chan.cals]
                chan.frame_num = chan.frame_num[np.logical_not(chan.cals)] - chan.ncals
                chan.packet_points = chan.data_pts[np.logical_not(chan.cals)]
                chan.sys_time = chan.sys_time[np.logical_not(chan.cals)]
                if gps_df.shape[0] > 1:
                    chan.pulse_time = np.interp(chan.sys_time, gps_df['systime'], gps_df.index)
                chan.atts = chan.atts[np.logical_not(chan.cals)]
                chan.nframes = len(chan.frame_num)
                if not is_df:
                    chan.pulse_length = int(chan.xml['Pulse_Length_S'] * chan.fs)

                # Add in overvoltaged pulses
                chan.overvoltage_pulses = list(set(np.arange(chan.nframes)) - set(chan.frame_num))

        if progress_tracker is not None:
            progress_tracker.set_description('Channel data finalized')

        super().__init__(fnme, files, xml, sys_mode, progress_tracker, self.n_channels, gim, ants, ports, ash, inscovs,
                         gimbal_df, gps_df, bestpos, timesync, dataversion, channels)

        # Now that channels are defined we can get a reference chirp
        for ch_idx, chan in enumerate(channels):
            if chan is None:
                continue
            if chan.is_receive_only:
                chan.cal_chirp = None
            else:
                chan.cal_chirp = np.mean(self.getPulses(chan.cal_num, ch_idx, is_cal=True)[1], axis=1)
            chan.matched_filter = None
            # This is a quick hack put in to fix simultaneous modes stuff
            try:
                if int(chan.wavenum) == 0 and not chan.is_receive_only:
                    chan.ref_chirp = waveforms[int(chan.wavenum) + 1]
                    chan.raw_ref_chirp = raw_waveforms[int(chan.wavenum) + 1]
                else:
                    chan.ref_chirp = waveforms[int(chan.wavenum)]
                    chan.raw_ref_chirp = raw_waveforms[int(chan.wavenum)]
            except KeyError:
                chan.ref_chirp = waveforms[int(chan.wavenum)]
                chan.raw_ref_chirp = raw_waveforms[int(chan.wavenum)]
            except Exception:
                # Put this in here as a catchall for earlier collects
                chan.ref_chirp = chan.cal_chirp
                chan.raw_ref_chirp = None

        if progress_tracker is not None:
            progress_tracker.set_description('Reference chirps calculated')


class Channel(object):
    overvoltage_pulses: np.array
    raw_ref_chirp: np.array
    rf_ref: np.array
    ref_chirp: np.array
    cal_chirp: np.array
    cal_num: np.array
    packet_points: np.array
    frame_num: np.array
    nsam: int = 0
    nframes: int
    ncals: int
    data_pts: np.array
    cal_pts: np.array
    cal_time: np.array
    sys_time: np.array
    cals: np.array
    atts: np.array
    wavenum: int
    pulse_time: np.array
    xml: dict
    is_old: bool
    mode: int
    matched_filter: np.ndarray
    pol: str

    def __init__(self,
                 xml: dict,
                 mode: int = None,
                 is_old: bool = False,
                 **kwargs):
        if mode is None:
            mode = xml['Mode']
            if isinstance(mode, str):
                mode = int(mode, 16)
            elif isinstance(mode, float):
                mode = int(f'{int(mode)}', 16)
        self.mode_settings = getModeValues(mode, is_old)
        self.mode = mode
        self.xml = xml
        self.is_old = is_old
        for key, val in kwargs.items():
            self.__dict__[key] = val
        self.pol = POLARIZATION[self.mode_settings['polarization']]

    def unwrapSystemTime(self):
        tmp_times = self.sys_time.copy()
        self.sys_time = np.zeros((len(tmp_times),))

        # Unwrap the system time
        jumps = 0
        try:
            self.sys_time[0] = tmp_times[0]
            for nn in range(1, len(tmp_times)):
                if tmp_times[nn] < tmp_times[nn - 1]:
                    jumps += 1
                self.sys_time[nn] = tmp_times[nn] + jumps * INT_MAX + jumps
        except IndexError:
            self.sys_time = []

    def idx(self, frame_num):
        if isinstance(frame_num, (list, np.ndarray)):
            return [self.frame_num.tolist().index(f) for f in frame_num]
        else:
            return self.frame_num.tolist().index(frame_num)

    @property
    def rec_num(self):
        return self.mode_settings['rx_port']

    @property
    def trans_num(self):
        return self.mode_settings['tx_port']

    @property
    def NCO_freq_Hz(self):
        return self.xml['NCO_Hz']

    @property
    def mixDownHz(self):
        return MIXDOWN_FREQS[self.mode_settings['band']] + 1e9 if self.mode_settings['8/9_select'] else MIXDOWN_FREQS[self.mode_settings['band']]

    @property
    def baseband_fc(self):
        return (self.xml['Center_Frequency_Hz'] - self.mixDownHz
                - abs(self.NCO_freq_Hz)) % BASE_COMPLEX_SRATE_HZ

    @property
    def transmit_on_TAC(self):
        return self.xml['Transmit_On_TAC']

    @property
    def transmit_off_TAC(self):
        return self.xml['Transmit_Off_TAC']

    @property
    def receive_on_TAC(self):
        return self.xml['Receive_On_TAC']

    @property
    def receive_off_TAC(self):
        return self.xml['Receive_Off_TAC']

    @property
    def isOffsetVideo(self):
        try:
            return self.xml['Offset_Video_Enabled'].lower() == 'true'
        except KeyError:
            return False

    @property
    def is_receive_only(self):
        try:
            return self.xml['Receive_Only'] == 'true'
        except KeyError:
            return True

    @property
    def prf(self):
        try:
            return self.xml['Effective_PRF_Hz']
        except KeyError:
            return self.xml['PRI_TAC'] / TAC

    @property
    def is_lpf(self):
        return True if self.fs < BASE_COMPLEX_SRATE_HZ else self.mode_settings['filter_select']

    @property
    def fs(self):
        return float(self.xml['Sampling_Frequency_Hz']) \
            if 'Sampling_Frequency_Hz' in self.xml else BASE_COMPLEX_SRATE_HZ

    @property
    def fc(self):
        return self.xml['Center_Frequency_Hz']

    @property
    def bw(self):
        try:
            return self.xml['Bandwidth_Hz']
        except KeyError:
            return 0

    @property
    def start_freq_hz(self):
        try:
            return self.fc - self.bw / 2 if self.xml['Chirp_Direction'] == 'Up' else self.fc + self.bw / 2
        except KeyError:
            return self.fc - self.bw / 2

    @property
    def stop_freq_hz(self):
        try:
            return self.fc + self.bw / 2 if self.xml['Chirp_Direction'] == 'Up' else self.fc - self.bw / 2
        except KeyError:
            return self.fc + self.bw / 2

    @property
    def pulse_length_S(self):
        # Compute the pulse length in seconds based on the transmit on and off
        #   in TAC to get the most accurate number without rounding error
        return (self.transmit_off_TAC - self.transmit_on_TAC) / TAC

    @property
    def pulse_length_N(self):
        return int(np.ceil(self.pulse_length_S * self.fs))

    @property
    def chirp_rate(self):
        return self.bw / self.pulse_length_S


@dataclass
class AntennaPort:
    x: float
    y: float
    z: float
    num: int
    assoc_ant: int = 0


@dataclass
class Antenna:
    az_bw: float
    el_bw: float
    dopp_bw: float
    dep_ang: float
    az_phase_centers: int
    el_phase_centers: int
    squint: float = 0
    is_split_weight: bool = False


def load(collection_path, pickle_path='', import_pickle=True, export_pickle=True, **kwargs):
    """Creates a SDRParse object from a .sar file and optionally a .pic file. The SDRParse will contain all
    the required information from a SlimSDR collection to perform furthur signal processing. The optional
    .pic file is used to increase speed. Loading a the .pic file prevents the parser from having to scan the
    entire .sar file in order to find start and stop words. The .sar file is still required to since it
    holds the needed pulse data, as the SDRParse object only holders pointers.

    Args:
        collection_path ([type]): path and filename of the collection to parse. If the collection_path ends with a '\\'
        it is assume to be a directory and the first .sar file found in the directory will be used.
        pickle_path ([type], optional): path to find the associated pickle for the collection. If set to None,
        the pickle file is assumed to be co-located with the .sar file. Defaults to None.
        progress_tracker ([type], optional): Object used for tracking progress within the HiLite web UI. Defaults to
        None.
        import_pickle (bool, optional): Specifies wheather or not to use an existing pickle file. Defaults to True.
        export_pickle (bool, optional): Specifies wheather or not to export the pickle file. Defaults to True.

    Returns:
        SARParse: Object that holds and/or maps all required information from a SlimSDR collection to perform furthur
        signal processing.
    """

    # find the first .sar file is a directory was provided
    cpath = Path(collection_path)
    ppath = Path(pickle_path)
    if cpath.is_dir():
        raise Exception('Please provide a .pic or a .sar file.')
    elif cpath.suffix == '.pic':
        with open(cpath, mode='r+b') as fid:
            pickle_sar = SDRBase(**pickle.load(fid))
    elif cpath.suffix == '.sar':
        if import_pickle:
            if ppath.is_file() and ppath.exists():
                with open(ppath, mode='r+b') as fid:
                    pickle_sar = SDRBase(**pickle.load(fid))
            elif Path(collection_path[:-4] + '.pic').exists():
                with open(collection_path[:-4] + '.pic', mode='r+b') as fid:
                    pickle_sar = SDRBase(**pickle.load(fid))
            else:
                pickle_sar = SDRParse(str(cpath), **kwargs)
        else:
            pickle_sar = SDRParse(str(cpath), **kwargs)

    if export_pickle:
        epath = str(ppath) if ppath.is_file() else collection_path[:-4] + '.pic'
        with open(epath, mode='wb') as fid:
            pickle.dump(pickle_sar.dump(), fid)

    return pickle_sar


def jumpCorrection(a_data):
    # a_data = rawGPS['alt'] + 0.0
    r_data = np.ones_like(a_data)
    grad = abs(np.gradient(np.gradient(a_data)))
    thresh = medfilt(grad, 15) + np.sqrt(np.cov(grad, aweights=1 / grad)) * 4
    indices = np.where(thresh < grad)[0]
    p_i = 0
    for i in indices:
        i += 2
        if i - p_i <= 1 or i + 1 not in indices:
            continue
        # half_i = p_i + (i - p_i) // 2
        r_data[p_i:i] = abs(abs(np.linspace(0, 1, i - p_i)) ** (1 / 6) - 1)
        p_i = i
    r_data = splev(np.arange(len(a_data)), splrep(np.arange(len(a_data)), a_data, w=r_data, s=grad.mean() / 1000))
    return r_data


def loadXMLFile(fnme: str, keep_structure: bool = False) -> dict:
    """
    Parse the xml header file ('ash') associated with the SAR image data to
    retrieve the relative parameters for the dataset
    """
    # instantiate an object of the W3C Document Object Model (DOM) using the ".ash" file for parsing the xml
    # The subapertures have a different name than full aperture SAR images
    dom = parse_xml(parse(fnme))
    try:
        data = dom['SlimSAR_Configuration']['SlimSAR_Info']
    except KeyError:
        data = dom['SlimSDR_Configuration']['SlimSDR_Info']
    return dom if keep_structure else flatten(data)


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, k, sep=sep).items())
        else:
            items.append((k, v))
    return dict(items)


def parse_xml(xml_object, param=None):
    """Extract and return key/value pairs from xml objects.  This method accepts a DOM xml object"""
    # Loop through each node of the DOM xml object and assign the key/value pairs to the dictionary
    if param is None:
        param = {}
    for node in xml_object.childNodes:
        if node.nodeType == node.ELEMENT_NODE:
            if sum(nn.nodeType == nn.TEXT_NODE for nn in node.childNodes) == 1:
                # if it has a child node, then we will extract the data
                if node.hasChildNodes():
                    # if the node value contains a letter A-Za-z then interpret it as a string
                    if search('[A-Za-z]|:', node.childNodes[0].nodeValue):
                        param[str(node.nodeName)] = str(node.childNodes[0].nodeValue)
                    # else interpret the node value as a float
                    else:
                        try:
                            param[str(node.nodeName)] = float(node.childNodes[0].nodeValue)
                        except ValueError:
                            param[str(node.nodeName)] = node.childNodes[0].nodeValue
                # else, if there is not a child node, then assign it an empty list
                else:
                    param[str(node.nodeName)] = []
            else:
                param[str(node.nodeName)] = parse_xml(node, {})
    return param


def loadASHFile(filename: str) -> dict:
    """
    Parse the xml header file ('ash') associated with the SAR image data to
    retrieve the relative parameters for the dataset
    """
    # instantiate an object of the W3C Document Object Model (DOM) using the ".ash" file for parsing the xml
    # The subapertures have a different name than full aperture SAR images
    dom = parse(filename)
    # The SARImage data type is assigned the xml base node
    datatype = str(dom.childNodes[0].nodeName)
    # pass _parse_xml the DOM object corresponding to each of the sub-parameter fields (or nodes)
    # to extract key/value pairs for a python dictionary data structure.  This is done for File_Parameters,
    # Image_Parameters, Geo_Parameters, Flight_Parameters, Radar_Parameters, Processing_Parameters.
    paramFile = parse_xml(dom.childNodes[0].childNodes[1], {})
    paramImage = parse_xml(dom.childNodes[0].childNodes[3], {})
    paramGeo = parse_xml(dom.childNodes[0].childNodes[5], {})
    paramFlight = parse_xml(dom.childNodes[0].childNodes[7], {})
    paramRadar = parse_xml(dom.childNodes[0].childNodes[9], {})
    paramProc = parse_xml(dom.childNodes[0].childNodes[11], {})
    return {'type': datatype, 'file': paramFile, 'image': paramImage, 'geo': paramGeo, 'flight': paramFlight,
            'radar': paramRadar, 'proc': paramProc}


def loadASIFile(filename, nrows=0, ncols=0, scale=1e10):
    """Read in the complex SAR image data and reshape it"""
    # we should really scale the data by a value
    # construct filename from the SAR image object path, name, and .asi
    # open the file for reading
    with open(filename, 'r') as fid:
        # read in the whole file binary data as type: complex64, and reshape to 2-dimensional numpy array
        check_ncols = np.fromfile(fid, dtype='uint32', count=1, sep="")[0]
        check_nrows = np.fromfile(fid, dtype='uint32', count=1, sep="")[0]
        if check_ncols == 0 or check_nrows == 0:
            if check_ncols != ncols and check_nrows != nrows:
                fid.seek(0)
        if ncols == 0 or nrows == 0:
            ncols = check_ncols
            nrows = check_nrows
        data = (np.fromfile(fid, dtype='complex64', count=-1, sep="")) / scale
        try:
            data = data.reshape((int(nrows), int(ncols)), order='C')
        except ValueError:
            fac = factors(len(data))
            if nrows in fac:
                data = data.reshape((int(nrows), int(len(data) / nrows)), order='C')
            elif ncols in fac:
                data = data.reshape((int(len(data) / ncols), int(ncols)), order='C')
            else:
                print('ASI file length not making sense.')
        # Return flipped so that it reads increasing in the range direction
        return np.flipud(np.array(data))


def findAllFilenames(sar_filepath, exact_matches=True):
    # Get all files in the given directory that match our sar_filename
    fls = glob(f'{sar_filepath[:-4]}.*') if exact_matches else glob(f'{sar_filepath[:-4]}*')
    # Remove any directories that may have slipped in
    fls = [f for f in fls if Path(f).is_file()]
    filelist = dict(zip([Path(f).suffix[1:] for f in fls], fls))
    return filelist


def findDebugFilenames(sar_filename, debug_dir):
    # Get all files in the given directory that match our sar_filename
    fls = glob(f'{debug_dir}/{sar_filename[:-4]}*.*')
    # Remove any directories that may have slipped in
    fls = [f for f in fls if Path(f).is_file()]
    filelist = {}
    for f in fls:
        if 'Channel' in f:
            tmp = f.split('Channel')[1].split('_')
            channel = int(tmp[1]) - 1
            ftype = tmp[-1].split('.')[0]
            if channel not in filelist.keys():
                filelist[channel] = {ftype: f}
            else:
                filelist[channel][ftype] = f
        else:
            filelist[f.split(sar_filename[:-4])[1].split('.')[0][1:]] = f
    return filelist


def findPowerOf2(x):
    if x < 0:
        return 0
    x |= x >> 1
    x |= x >> 2
    x |= x >> 4
    x |= x >> 8
    x |= x >> 16
    return x + 1


def window_taylor(N, nbar=4, sll=-30):
    """Taylor tapering window
    Taylor windows allows you to make tradeoffs between the
    mainlobe width and sidelobe level (sll).
    Implemented as described by Carrara, Goodman, and Majewski
    in 'Spotlight Synthetic Aperture Radar: Signal Processing Algorithms'
    Pages 512-513
    :param N: window length
    :param float nbar:
    :param float sll:
    The default values gives equal height
    sidelobes (nbar) and maximum sidelobe level (sll).
    .. warning:: not implemented
    .. seealso:: :func:`create_window`, :class:`Window`
    """
    if sll > 0:
        sll *= -1
    B = 10 ** (-sll / 20)
    A = np.log(B + np.sqrt(B ** 2 - 1)) / np.pi
    s2 = nbar ** 2 / (A ** 2 + (nbar - 0.5) ** 2)
    ma = np.arange(1, nbar)

    def calc_Fm(m):
        numer = (-1) ** (m + 1) \
                * np.prod(1 - m ** 2 / s2 / (A ** 2 + (ma - 0.5) ** 2))
        denom = 2 * np.prod([1 - m ** 2 / j ** 2 for j in ma if j != m])
        return numer / denom

    Fm = np.array([calc_Fm(m) for m in ma])

    def W(n):
        return 2 * np.sum(
            Fm * np.cos(2 * np.pi * ma * (n - N / 2 + 1 / 2) / N)) + 1

    w = np.array([W(n) for n in range(N)])
    # normalize (Note that this is not described in the original text)
    scale = W((N - 1) / 2)
    w /= scale
    return w


if __name__ == '__main__':
    # Script testing.

    slimsdr_fnme = '/data6/SAR_DATA/2024/09062024/SAR_09062024_104353.sar'  # DF
    # slimsdr_fnme = '/home/jeff/Downloads/SAR_03112024_211226.sar'  # SAR

    # stime = time.time()
    # sart = load(slimsdr_fnme, skip_gps=True, import_pickle=False, export_pickle=True, use_jump_correction=False)
    sart = load(slimsdr_fnme, export_pickle=False)
    sar = SDRParse(slimsdr_fnme, progress_tracker=True, use_jump_correction=False)
    # sar0 = SDRParse('/home/jeff/SDR_DATA/RAW/04112024/SAR_04112024_095932.sar', progress_tracker=True,
    #                use_jump_correction=False)
