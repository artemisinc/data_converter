import numpy as np
import os
from pathlib import Path
import datetime
import struct
from scipy.interpolate import CubicSpline
from SDRParsing import SDRParse, getModeValues, load
from bitarray import bitarray
from bitarray.util import ba2int, int2ba
from tqdm import tqdm
from xml.etree.ElementTree import ElementTree, Element, SubElement, indent, tostringlist
from xml.dom.minidom import parse
import matplotlib.pyplot as plt

freq_offset = 5e6
DTR = np.pi / 180
mstoknots = 1.94384
c0 = 299792458.0
TAC = 125e6

# The real valued sampling frequency of the DAC
DAC_FREQ_HZ = 4e9
BASE_COMPLEX_SRATE_HZ = DAC_FREQ_HZ / 2
SHRT_MAX = 32767
USHRT_MAX = 65535
INT_MAX = 4294967295
pulseChunkMax = USHRT_MAX * 10000  # No more than 10000 full size pulses per loop
DSCALE = .75 * SHRT_MAX  # * 2e16
# Pan and tilt conversion factors for the gimbal E46_70
GIM_CONV = {'FLIR D48': {'pan': 23.142857 / 3600, 'tilt': 11.571429 / 3600},
            'FLIR E46-70': {'pan': -23.142857 / 3600, 'tilt': 11.571429 / 3600}}
DATA_START_WORD = b'\x0A\xB0\xFF\x18'
GPS_START_WORD = b'\x0A\xFF\xB0\x18'
GIMBAL_START_WORD = b'\x0A\xFF\x18\xB0'
WAVEFORM_START_WORD = b'\x0A\xB0\xFF\x45'
STOP_WORD = b'\xAA\xFF\x11\x55'
COLLECTION_MODE_OLD_DIGITAL_CHANNEL_MASK = 0x000003E000000000
COLLECTION_MODE_DIGITAL_CHANNEL_MASK = 0x000000F800000000
COLLECTION_MODE_WAVENUMBER_MASK = 0x0000070000000000
COLLECTION_MODE_OLD_WAVENUMBER_MASK = 0x00001C0000000000
COLLECTION_MODE_OPERATION_MASK = 0x1800000000000000
COLLECTION_MODE_DIGITAL_CHANNEL_SHIFT = 35
COLLECTION_MODE_OLD_DIGITAL_CHANNEL_SHIFT = 37
COLLECTION_MODE_WAVENUMBER_SHIFT = 40
COLLECTION_MODE_OLD_WAVENUMBER_SHIFT = 42
COLLECTION_MODE_OPERATION_SHIFT = 59
OVERVOLTAGE_MASK = 0x80

# Used to calculate out mode bit stuff
MODE_SHIFTS = {'digital_channel': [37, 35], 'wavenumber': [42, 40],
               'operation': 59, 'adc_channel': 61, 'receiver_slot': 56, 'receiver_channel': 55, 'upconverter_slot': 52,
               '8/9_select': 51, 'band': [49, 47], 'dac_channel': [46, 44],
               'ddc_enable': [36, 34], 'filter_select': [33, 31],
               'rx_port': [29, 27], 'tx_port': [25, 23], 'polarization': [23, 21],
               'numconsmodes': [20, 18], 'awg_enable': [18, 17],
               'rf_ref_wavenumber': 13}
MODE_SZ = {'digital_channel': [4, 4], 'wavenumber': [3, 3],
           'operation': 2, 'adc_channel': 3, 'receiver_slot': 3, 'receiver_channel': 1, 'upconverter_slot': 3,
           '8/9_select': 1, 'band': [2, 4], 'dac_channel': [3, 3],
           'ddc_enable': [1, 1], 'filter_select': [3, 3],
           'rx_port': [4, 4], 'tx_port': [4, 4], 'polarization': [2, 2],
           'numconsmodes': [3, 3], 'awg_enable': [1, 1],
           'rf_ref_wavenumber': 4}
MODE_BIT_MASK = {'digital_channel': [2061584302080, 515396075520], 'wavenumber': [30786325577728, 7696581394432],
                 'operation': 1729382256910270464, 'adc_channel': 16140901064495857664,
                 'receiver_slot': 504403158265495552, 'receiver_channel': 36028797018963968,
                 'upconverter_slot': 31525197391593472, '8/9_select': 2251799813685248,
                 'band': [1688849860263936, 2111062325329920], 'dac_channel': [492581209243648, 123145302310912],
                 'ddc_enable': [68719476736, 17179869184], 'filter_select': [60129542144, 15032385536],
                 'rx_port': [8053063680, 2013265920], 'tx_port': [503316480, 125829120],
                 'polarization': [25165824, 6291456], 'numconsmodes': [7340032, 1835008],
                 'awg_enable': [262144, 131072], 'rf_ref_wavenumber': 122880}
CAL_SAR_SHIFT = 1024
REC_CHUNK_SZ = 4096
SDR_HEADER_SZ = 28
SAR_HEADER_SZ = 24


def datetime_to_tow(t):
    """
    Convert a Python datetime object to GPS Week and Time Of Week.
    Does *not* convert from UTC to GPST.
    Fractional seconds are supported.

    Parameters
    ----------
    t : datetime
      A time to be converted, on the GPST timescale.
    mod1024 : bool, optional
      If True (default), the week number will be output in 10-bit form.

    Returns
    -------
    week, tow : tuple (int, float)
      The GPS week number and time-of-week.
    """
    # DateTime to GPS week and TOW
    wk_ref = datetime.datetime(2014, 2, 16, 0, 0, 0, 0, None)
    refwk = 1780
    wk = (t - wk_ref).days // 7 + refwk
    tow = ((t - wk_ref) - datetime.timedelta((wk - refwk) * 7.0)).total_seconds()
    return wk, tow


"""
SDRWrite

class to write simulated data to a .sar file

This class expects a complete GPS and gimbal dataset
in order to place them in the right spots.
"""


def wrap(t):
    return int(t % INT_MAX)


class SDRWrite(object):
    _inspvas_interval = 0.01
    _gimbal_interval = 0.01
    _bpts_interval = 1
    _next_bpts = 0
    _next_inspvas = 0
    _next_gimbal = 0
    _curr_systime = 0
    _curr_time = 0
    _curr_ppstime = 0
    _frame = 0
    _is_gimbal = False
    n_channels = 0
    n_cals = 0

    def __init__(self, output_fnme, gpswk, times, lats, lons, alts, rolls, pitchs, yaws, vn, ve, vu, gimbal=None,
                 settings_alt=1524,
                 settings_vel=150, gimbal_type='FLIR E46-70', start_flight_line=None, stop_flight_line=None,
                 waveform=None, fs=2e9):
        self._fnme = output_fnme
        with open(output_fnme, 'wb') as f:
            f.write(struct.pack('b', 1))  # Data version of one
            f.write(struct.pack('b', 1))  # System code for SlimSDR

        self._init_gpstime = (gpswk, min(times[0], gimbal[0, 0]))
        self.fs = fs
        self.is_lowpass = True if self.fs < 2e9 else False

        self._next_bpts = np.ceil(self._init_gpstime[1])
        self._next_inspvas = times[0]
        self._next_gimbal = gimbal[0, 0]
        self._init_systime = 0 % INT_MAX
        self._prev_time = self._init_gpstime[1]

        self.lat = CubicSpline(times, lats)
        self.lon = CubicSpline(times, lons)
        self.alt = CubicSpline(times, alts)
        self.roll = CubicSpline(times, rolls)
        self.pitch = CubicSpline(times, pitchs)
        self.yaw = CubicSpline(times, yaws)
        self.vn = CubicSpline(times, ve)
        self.ve = CubicSpline(times, vn)
        self.vu = CubicSpline(times, vu)

        self._channel_mode = []
        self._cal_mode = []

        if gimbal is not None:
            self._is_gimbal = True
            self.pan = CubicSpline(gimbal[:, 0], gimbal[:, 1])
            self.tilt = CubicSpline(gimbal[:, 0], gimbal[:, 2])
            self._gimbal_type = gimbal_type
            self._next_gimbal = self._init_gpstime[1]
        
        self.xml = self.init_xml(settings_alt, settings_vel, start_flight_line=start_flight_line,
                                 stop_flight_line=stop_flight_line)

    @staticmethod
    def calcMode(digital_channel=0, wavenumber=0,
                 operation=0, adc_channel=0, receiver_slot=0, receiver_channel=0, upconverter_slot=0, select_89=0,
                 band=0, dac_channel=0, ddc_enable=0, filter_select=0, rx_port=0, tx_port=0, polarization=0,
                 numconsmodes=0, awg_enable=0, rf_ref_wavenumber=0):
        mode = 0
        mode_vals = {'digital_channel': digital_channel, 'wavenumber': wavenumber,
                     'operation': operation, 'adc_channel': adc_channel, 'receiver_slot': receiver_slot,
                     'receiver_channel': receiver_channel, 'upconverter_slot': upconverter_slot,
                     '8/9_select': select_89, 'band': band, 'dac_channel': dac_channel,
                     'ddc_enable': ddc_enable, 'filter_select': filter_select,
                     'rx_port': rx_port, 'tx_port': tx_port, 'polarization': polarization,
                     'numconsmodes': numconsmodes, 'awg_enable': awg_enable,
                     'rf_ref_wavenumber': rf_ref_wavenumber}
        for key, val in MODE_SHIFTS.items():
            if isinstance(val, list):
                mode += mode_vals[key] << val[1]
            else:
                mode += mode_vals[key] << val
        return int(mode)

    def writeBESTPOS(self):
        """
        Writes out a BESTPOS packet to file.
        :return: True if successful.
        """
        try:
            pd = []
            with open(self._fnme, 'ab') as f:
                pd.append(GPS_START_WORD)
                pd.append(struct.pack('>I', wrap(self._curr_ppstime)))  # PPSTIME
                # GPS Long Header
                pd.append(bytes([170, 68, 18]))  # Binary header for long header GPS
                pd.append(struct.pack('<b', 28))  # Header length
                pd.append(struct.pack('<H', 42))  # Message ID
                pd.append(struct.pack('<H', 0))  # message type/port address
                pd.append(struct.pack('<H', 76))  # Message length
                pd.append(struct.pack('<I', 0))  # sequence number, idle time, time status
                pd.append(struct.pack('<H', self._init_gpstime[0]))
                pd.append(struct.pack('<I', int(self._curr_time * 1000)))
                pd.append(struct.pack('<I', 0))  # Rec status
                pd.append(struct.pack('<H', 0))  # reserved vals
                pd.append(struct.pack('<H', 0))  # receiver s/w version
                # End Header
                pd.append(struct.pack('<I', 0))  # Solution status (zero for good)
                pd.append(struct.pack('<I', 56))  # Position type (zero for good)
                pd.append(struct.pack('<d', self.lat(self._curr_time)))
                pd.append(struct.pack('<d', self.lon(self._curr_time)))
                und = undulationEGM96(self.lat(self._curr_time), self.lon(self._curr_time))
                pd.append(struct.pack('<d', self.alt(self._curr_time) - und))
                pd.append(struct.pack('<f', und))
                pd.append(struct.pack('<I', 61))  # WGS84 datum id
                pd.append(struct.pack('<f', .01))  # Latitude std
                pd.append(struct.pack('<f', .01))  # Longitude std
                pd.append(struct.pack('<f', .01))  # Altitude std
                pd.append(struct.pack('<I', 0))  # Station ID
                pd.append(struct.pack('<f', 1))  # Differential age in seconds
                pd.append(struct.pack('<f', 1))  # Solution age in secs
                pd.append(struct.pack('<B', 9))  # Sats tracked
                pd.append(struct.pack('<B', 9))  # Sats in solution
                pd.append(struct.pack('<B', 9))  # Sats with l1 signals
                pd.append(struct.pack('<B', 9))  # Sats with multifrequency signals
                pd.append(struct.pack('<b', 0))  # Reserved
                pd.append(struct.pack('<b', 0))  # Ext Sol Status
                pd.append(struct.pack('<b', 0))  # Galileo mask
                pd.append(struct.pack('<b', 5))  # GPS mask
                pd.append(struct.pack('<f', und))  # Don't know why this needs to be here
                pd.append(struct.pack('<I', generateCRC(b''.join(pd[2:]))))
                pd.append(STOP_WORD)
                for p in pd:
                    f.write(p)
            return True
        except FileNotFoundError:
            return False

    def writeINSPVAS(self):
        """
        Writes out an INSPVAS packet to file.
        :return: True if successful.
        """
        try:
            pd = []
            with open(self._fnme, 'ab') as f:
                pd.append(GPS_START_WORD)
                pd.append(struct.pack('>I', wrap(self._curr_ppstime)))  # PPSTIME
                # Novatel Short Header
                pd.append(bytes([170, 68, 19]))  # Binary header for short header GPS
                pd.append(struct.pack('<b', 88))  # Short Message length
                pd.append(struct.pack('<H', 508))  # MessageID
                pd.append(struct.pack('<H', self._init_gpstime[0]))
                pd.append(struct.pack('<I', int(self._curr_time * 1000)))
                # End Short Header
                pd.append(struct.pack('<I', self._init_gpstime[0]))
                pd.append(struct.pack('<d', self._curr_time))
                pd.append(struct.pack('<d', self.lat(self._curr_time)))
                pd.append(struct.pack('<d', self.lon(self._curr_time)))
                pd.append(struct.pack('<d', self.alt(self._curr_time)))
                pd.append(struct.pack('<d', self.vn(self._curr_time)))
                pd.append(struct.pack('<d', self.ve(self._curr_time)))
                pd.append(struct.pack('<d', self.vu(self._curr_time)))
                pd.append(struct.pack('<d', self.roll(self._curr_time) / DTR))
                pd.append(struct.pack('<d', self.pitch(self._curr_time) / DTR))
                pd.append(struct.pack('<d', self.yaw(self._curr_time) / DTR))
                pd.append(struct.pack('<I', 3))  # Status
                pd.append(struct.pack('<I', generateCRC(b''.join(pd[2:]))))  # CRC
                pd.append(STOP_WORD)
                for p in pd:
                    f.write(p)
            return True
        except FileNotFoundError:
            return False

    def writeTIMESYNC(self):
        """
        Writes out a TIMESYNC packet to file.
        :return: True if successful.
        """
        try:
            pd = []
            with open(self._fnme, 'ab') as f:
                pd.append(GPS_START_WORD)
                pd.append(struct.pack('>I', wrap(self._curr_ppstime)))  # PPSTIME
                # GPS Long Header
                pd.append(bytes([170, 68, 18]))  # Binary header for long header GPS
                pd.append(struct.pack('<b', 28))  # Header length
                pd.append(struct.pack('<H', 492))  # Message ID
                pd.append(struct.pack('<H', 0))  # message type/port address
                pd.append(struct.pack('<H', 12))  # Message length
                pd.append(struct.pack('<I', 0))  # sequence number, idle time, time status
                pd.append(struct.pack('<H', self._init_gpstime[0]))
                pd.append(struct.pack('<I', int(self._curr_time * 1000)))
                pd.append(struct.pack('<I', 0))  # Rec status
                pd.append(struct.pack('<H', 0))  # reserved vals
                pd.append(struct.pack('<H', 0))  # receiver s/w version
                # End Header
                pd.append(struct.pack('<l', self._init_gpstime[0]))
                pd.append(struct.pack('<l', int(self._curr_time * 1000)))
                pd.append(struct.pack('<I', 0))  # Time status
                pd.append(struct.pack('<I', generateCRC(b''.join(pd[2:]))))  # CRC
                pd.append(STOP_WORD)
                for p in pd:
                    f.write(p)
            return True
        except FileNotFoundError:
            return False

    def writePulse(self, time, att, pul_data, channel=0, is_cal=False):
        """
        Writes out a data pulse to file.
        :param time: float time in seconds since the start of the collect
        :param att: int Attenuation from AGC, value 1-31.
        :param pul_data: complex double array of pulse data.
        :param is_cal: bool if True, writes as a calibration pulse.
        :return: True if successful.
        """

        # First, check to see if any of the other packets needs to be written
        if np.floor(time) - np.floor(self._prev_time) > 0:
            self._curr_time = np.floor(time)
            self._curr_systime = self._init_systime + int((self._curr_time - self._init_gpstime[1]) * TAC)
            self._curr_ppstime = self._init_systime + int((np.floor(time) - self._init_gpstime[1]) * TAC)
            self._next_bpts = np.round(self._next_bpts + self._bpts_interval)
            if not self.writeBESTPOS():
                print('BESTPOS failed to write.')
            if not self.writeTIMESYNC():
                print('TIMESYNC failed to write.')
        while time >= self._next_inspvas:
            self._curr_time = self._next_inspvas
            self._curr_systime = self._init_systime + int((self._curr_time - self._init_gpstime[1]) * TAC)
            self._next_inspvas = np.round(self._next_inspvas + self._inspvas_interval, 2)
            if not self.writeINSPVAS():
                print('INSPVAS failed to write.')
        while time >= self._next_gimbal:
            self._curr_time = self._next_gimbal
            self._curr_systime = self._init_systime + int((self._curr_time - self._init_gpstime[1]) * TAC)
            self._next_gimbal = np.round(self._next_gimbal + self._gimbal_interval, 2)
            if not self.writeGimbal():
                print('Gimbal packet failed to write.')

        # Now move to the pulse data
        self._curr_time = time
        self._curr_systime = self._init_systime + int((self._curr_time - self._init_gpstime[1]) * TAC)
        try:
            with open(self._fnme, 'ab') as f:
                f.write(DATA_START_WORD)
                # Write out the header for the pulse packet
                f.write(struct.pack('>I', self._frame))
                f.write(struct.pack('>I', wrap(self._curr_systime)))  # PPSTIME
                f.write(struct.pack('>Q', self._cal_mode[channel] if is_cal else self._channel_mode[channel]))
                f.write(struct.pack('>I', len(pul_data)))
                f.write(struct.pack('>B', int(att)))
                for _ in range(7):
                    f.write(struct.pack('b', 0))
                # Convert the pulse data into int16
                wr_data = np.zeros((len(pul_data) * 2,), dtype=np.int16)
                wr_data[0::2] = (pul_data.real / 10 ** (att / 20)).astype(np.int16)
                wr_data[1::2] = (pul_data.imag / 10 ** (att / 20)).astype(np.int16)
                wr_data.byteswap().tofile(f)
                f.write(STOP_WORD)
            self._frame += 1
            self.n_cals += 1 if is_cal else 0
            self._prev_time = time
            return True
        except FileNotFoundError:
            return False

    def writeGimbal(self):
        """
        Writes a gimbal packet to file.
        :return: True if successful.
        """
        try:
            with open(self._fnme, 'ab') as f:
                f.write(GIMBAL_START_WORD)
                f.write(struct.pack('>I', wrap(self._curr_systime)))  # PPSTIME
                f.write(struct.pack('>h', int(min(SHRT_MAX,
                                              max(-SHRT_MAX - 1,
                                                  np.round(self.pan(self._curr_time) / GIM_CONV[self._gimbal_type]['pan'] / DTR))))))
                f.write(struct.pack('>h', int(min(SHRT_MAX,
                                              max(-SHRT_MAX - 1,
                                                  np.round(self.tilt(self._curr_time) / GIM_CONV[self._gimbal_type][
                                                      'tilt'] / DTR))))))
                f.write(STOP_WORD)
            return True
        except FileNotFoundError:
            return False

    def writeWaveform(self, wave, wavenumber):
        try:
            with open(self._fnme, 'ab') as f:
                f.write(WAVEFORM_START_WORD)
                f.write(struct.pack('>b', wavenumber))
                f.write(struct.pack('>I', len(wave)))
                for n in wave:
                    f.write(struct.pack('h', int(n)))
                f.write(STOP_WORD)
            return True
        except FileNotFoundError:
            return False

    def init_xml(self, settings_alt, settings_vel, start_flight_line=None, stop_flight_line=None):
        # Create the basic structure of XML file.
        # This will be filled in later
        xml = Element('SlimSDR_Configuration')
        name = SubElement(xml, 'Configuration_Name')
        name.text = self._fnme

        ''' STRIDER INFO '''
        sinfo = SubElement(xml, 'Strider_Info')
        sinfo.insert(0, Element('Version'))
        sinfo[0].text = '2.3'
        sinfo.insert(1, Element('Build_Date'))
        sinfo[1].text = 'Aug   5  2021'
        sinfo.insert(2, Element('Build_Time'))
        sinfo[2].text = '09:57:11'

        ''' SLIMSDR INFO'''
        sdr_info = SubElement(xml, 'SlimSDR_Info')
        sdr_info.insert(0, Element('SlimSDR_Model'))
        sdr_info[0].text = 'SlimSDRDefault'
        sdr_info.insert(1, Element('Version'))
        sdr_info[1].text = '2.3'
        sdr_info.insert(2, Element('From_Collection'))
        sdr_info[2].text = 'true'
        sdr_info.insert(3, Element('System_Mode'))
        sdr_info[3].text = 'SAR'
        if start_flight_line is not None:
            sdr_info.insert(4, Element('Flight_Line'))
            sdr_info[4].insert(0, Element('Flight_Line_Type'))
            sdr_info[4][0].text = 'Straight'
            sdr_info[4].insert(1, Element('Flight_Line_Altitude_M'))
            sdr_info[4][1].text = f'{settings_alt:.2f}'
            sdr_info[4].insert(2, Element('Start_Latitude_D'))
            sdr_info[4][2].text = f'{start_flight_line[0]:.8f}'
            sdr_info[4].insert(3, Element('Start_Longitude_D'))
            sdr_info[4][3].text = f'{start_flight_line[1]:.8f}'
            sdr_info[4].insert(4, Element('Stop_Latitude_D'))
            sdr_info[4][4].text = f'{stop_flight_line[0]:.8f}'
            sdr_info[4].insert(5, Element('Stop_Longitude_D'))
            sdr_info[4][5].text = f'{stop_flight_line[1]:.8f}'
            self._common_channel_insert = 6
        else:
            self._common_channel_insert = 5
        sdr_info.insert(self._common_channel_insert - 1, Element('Gimbal_Settings'))
        sdr_info.insert(self._common_channel_insert, Element('Common_Channel_Settings'))
        sdr_info[self._common_channel_insert].insert(0, Element('Altitude_Ft'))
        sdr_info[self._common_channel_insert].insert(1, Element('Velocity_Knots'))
        sdr_info[self._common_channel_insert].insert(2, Element('Num_Cal_Pulses'))
        sdr_info[self._common_channel_insert].insert(3, Element('Circle_SAR_Enabled'))
        sdr_info[self._common_channel_insert][3].text = 'false'
        sdr_info[self._common_channel_insert].insert(4, Element('PRF_Broadening_Factor'))
        sdr_info[self._common_channel_insert][4].text = '1.5'
        sdr_info[self._common_channel_insert].insert(5, Element('GMTI'))
        sdr_info[self._common_channel_insert][5].text = 'false'
        sdr_info[self._common_channel_insert].insert(6, Element('PRI_TAC'))
        sdr_info[self._common_channel_insert].insert(7, Element('Transmit_PRF_Hz'))
        sdr_info[self._common_channel_insert].insert(8, Element('Num_Digital_Channels'))
        sdr_info[self._common_channel_insert].insert(9, Element('Antenna_Settings'))
        if self.fs != 2e9:
            sdr_info[self._common_channel_insert].insert(10, Element('Low_Pass_Filter_1'))
            sdr_info[self._common_channel_insert][10].insert(0, Element('Decimation_Factor'))
            sdr_info[self._common_channel_insert][10][0].text = f'{2e9 // self.fs:.0f}'
            sdr_info[self._common_channel_insert][10].insert(1, Element('Pass_Band_Freq_MHz'))
            sdr_info[self._common_channel_insert][10][1].text = '0'
            sdr_info[self._common_channel_insert][10].insert(2, Element('Stop_Band_Freq_MHz'))
            sdr_info[self._common_channel_insert][10][2].text = f'{self.fs / 2}'
            sdr_info[self._common_channel_insert][10].insert(3, Element('LPF_Coefficients'))
            # These coefficients are MADE UP, they mean nothing
            sdr_info[self._common_channel_insert][10][3].text = '0.0039215087890625 0.0060577392578125 ' \
                                                                '0.0083007812500000 0.0106201171875000 ' \
                                                                '0.0129699707031250 0.0153045654296875 ' \
                                                                '0.0175781250000000 0.0197601318359375 ' \
                                                                '0.0218048095703125 0.0236663818359375 ' \
                                                                '0.0253143310546875 0.0267028808593750 ' \
                                                                '0.0278167724609375 0.0286254882812500 ' \
                                                                '0.0291290283203125 0.0292816162109375 ' \
                                                                '0.0291290283203125 0.0286254882812500 ' \
                                                                '0.0278167724609375 0.0267028808593750 ' \
                                                                '0.0253143310546875 0.0236663818359375 ' \
                                                                '0.0218048095703125 0.0197601318359375 ' \
                                                                '0.0175781250000000 0.0153045654296875 ' \
                                                                '0.0129699707031250 0.0106201171875000 ' \
                                                                '0.0083007812500000 0.0060577392578125 ' \
                                                                '0.0039215087890625 '

        xml_info = ElementTree(xml)

        ch_s = xml_info.getroot()[2][self._common_channel_insert]  # Common_Channel_Settings
        ch_s[0].text = f'{settings_alt * 3.2808}'
        ch_s[1].text = f'{settings_vel:.2f}'
        
        return xml_info

    def addChannel(self, fc, bandwidth, pulse_length, dopp_prf, near_range, far_range, pulse_length_percent,
                   presum, trans_on_tac=1000, att_mode='AGC', rec_only=False, rec_slice='A', prf_broad=1.5, fs=2e9,
                   digital_channel=0, wavenumber=0, adc_channel=0, receiver_slot=0, receiver_channel=0,
                   upconverter_slot=0, select_89=0, band=0, dac_channel=0, ddc_enable=0, filter_select=0, rx_port=0,
                   tx_port=0, polarization=0, numconsmodes=0, awg_enable=0, rf_ref_wavenumber=0,
                   offset_video=5e6, waveform=None, *args, **kwargs):
        """
        Adds all the necessary information to create a channel in XML file.
        :param fc: float Center frequency of channel.
        :param bandwidth: float Bandwidth of channel.
        :param pulse_length: float Pulse length of channel, taking into account pulse length percent.
        :param dopp_prf: float Doppler PRF of channel.
        :param near_range: float Near range of channel, in degrees.
        :param far_range: float Far range of channel, in degrees.
        :param pulse_length_percent: float Pulse length percent.
        :param presum: int Presum factor of channel.
        :param trans_on_tac: int Transmit on, in TAC. Usually 1000.
        :param att_mode: str Attenuation mode. Hard coded to AGC for now.
        :param rec_only: bool Flag for Receive Only collect. Should always be False.
        :return: Happiness and joy.
        """

        # Calculate out mode bits for this channel
        # Normal operation
        while len(self._channel_mode) < digital_channel + 1:
            self._channel_mode.append(None)
            self._cal_mode.append(None)
        self._channel_mode[digital_channel] = self.calcMode(digital_channel, wavenumber,
                                           0, adc_channel, receiver_slot, receiver_channel, upconverter_slot, select_89,
                                           band, dac_channel, ddc_enable, filter_select, rx_port, tx_port, polarization,
                                           numconsmodes, awg_enable, rf_ref_wavenumber)

        # Calibration mode
        self._cal_mode[digital_channel] = self.calcMode(digital_channel, wavenumber,
                                       1, adc_channel, receiver_slot, receiver_channel, upconverter_slot, select_89,
                                       band, dac_channel, ddc_enable, filter_select, rx_port, tx_port, polarization,
                                       numconsmodes, awg_enable, rf_ref_wavenumber)

        mode_vals = getModeValues(self._channel_mode[digital_channel])
        altitude = float(self.xml.getroot()[2][self._common_channel_insert][0].text) / 3.2808
        spot = len(self.xml.getroot()[2])

        self.xml.getroot()[2].insert(
            spot, Element(f'Channel_{mode_vals["digital_channel"]}'))

        # There's a lot of stuff here.
        chan = self.xml.getroot()[2][spot]
        text_band = 'X-Band' if 8e9 < fc < 12e9 else 'Ka-Band' if 26.5e9 < fc < 40e9 \
            else 'L-Band' if 1e9 < fc < 2e9 else 'UHF'
        bboffset = 8e9 if text_band == 'X-Band' else 33e9
        upper_band_en = True if text_band == 'X-Band' and fc > 9e9 else text_band == 'Ka-Band' and fc > 34e9
        bboffset += 1e9 if upper_band_en else 0
        nco_hz = bboffset - fc + bandwidth / 2 + offset_video if offset_video != 0 else bboffset - fc
        rfi_enable = False
        nxt = 0

        chan.insert(nxt, Element('Freq_Band'))
        chan[nxt].text = text_band
        nxt += 1
        chan.insert(nxt, Element('Mode'))
        chan[nxt].text = f'{np.uint64(self._channel_mode[digital_channel]):016x}'.upper()
        nxt += 1
        chan.insert(nxt, Element('Waveform_Upload'))
        chan[nxt].text = 'true' if mode_vals['awg_enable'] else 'false'
        nxt += 1
        chan.insert(nxt, Element('Center_Frequency_Hz'))
        chan[nxt].text = f'{fc:f}'
        nxt += 1
        chan.insert(nxt, Element('Bandwidth_Hz'))
        chan[nxt].text = f'{bandwidth:f}'
        nxt += 1
        chan.insert(nxt, Element('Offset_Video_Enabled'))
        if offset_video != 0:
            chan[nxt].text = 'True'
            nxt += 1
            chan.insert(nxt, Element('DC_Offset_MHz'))
            chan[nxt].text = f'{offset_video / 1e6:f}'
        else:
            chan[nxt].text = 'False'
        nxt += 1
        chan.insert(nxt, Element('Chirp_Direction'))
        chan[nxt].text = 'Up'
        nxt += 1
        chan.insert(nxt, Element('NCO_Hz'))
        chan[nxt].text = f'{nco_hz}'
        nxt += 1
        chan.insert(nxt, Element('Near_Range_D'))
        chan[nxt].text = f'{near_range:.2f}'
        nxt += 1
        chan.insert(nxt, Element('Far_Range_D'))
        chan[nxt].text = f'{far_range:.2f}'
        nxt += 1
        chan.insert(nxt, Element('Pulse_Length_Percent'))
        chan[nxt].text = f'{pulse_length_percent:.2f}'
        nxt += 1
        chan.insert(nxt, Element('Presum_Factor'))
        chan[nxt].text = f'{presum}'
        nxt += 1
        chan.insert(nxt, Element('Attenuation_Mode'))
        chan[nxt].text = att_mode
        nxt += 1
        if att_mode == 'AGC':
            chan.insert(nxt, Element('AGC_Settings'))

            # AGC Settings
            nxt1 = 0
            chan[nxt].insert(nxt1, Element('Initial_Attenuation_dB'))
            chan[nxt][nxt1].text = '18'
            chan[nxt].insert(nxt1, Element('Max_Threshold_Percent'))
            chan[nxt][nxt1].text = '70'
            chan[nxt].insert(nxt1, Element('Min_Threshold_Percent'))
            chan[nxt][nxt1].text = '50'
            chan[nxt].insert(nxt1, Element('Max_Threshold_Count'))
            chan[nxt][nxt1].text = '10'
            chan[nxt].insert(nxt1, Element('Min_Threshold_Count'))
            chan[nxt][nxt1].text = '100'
        elif att_mode == 'Static':
            chan.insert(nxt, Element('Static_Attenuation_dB'))
            chan[nxt].text = '21'
            nxt += 1
        chan.insert(nxt, Element('RFI_Mitigation_Enabled'))
        if rfi_enable:
            chan[nxt].text = 'true'
            nxt += 1
            chan.insert(nxt, Element('RFI_Mitigation'))
        else:
            chan[nxt].text = 'false'
        nxt += 1
        chan.insert(nxt, Element('Upper_Band_Enabled'))
        chan[nxt].text = 'true' if upper_band_en else 'false'
        nxt += 1
        chan.insert(nxt, Element('Receive_Only'))
        chan[nxt].text = 'true' if rec_only else 'false'
        nxt += 1
        chan.insert(nxt, Element('Sampling_Frequency_Hz'))
        chan[nxt].text = f'{fs}'
        nxt += 1
        if self.is_lowpass:
            chan.insert(nxt, Element('Channel_Low_Pass_Filter_Number'))
            chan[nxt].text = '1'
            nxt += 1
        chan.insert(nxt, Element('Upconverter_Slot'))
        chan[nxt].text = f"{mode_vals['upconverter_slot']}"
        nxt += 1
        chan.insert(nxt, Element('Receiver_Slot'))
        chan[nxt].text = f"{mode_vals['receiver_slot']}"
        nxt += 1
        chan.insert(nxt, Element('Receiver_Slice'))
        chan[nxt].text = rec_slice
        nxt += 1
        chan.insert(nxt, Element('Transmit_Port'))
        chan[nxt].text = f"{mode_vals['tx_port']}"
        nxt += 1
        chan.insert(nxt, Element('Receive_Port'))
        chan[nxt].text = f"{mode_vals['rx_port']}"
        nxt += 1
        chan.insert(nxt, Element('Pulse_Length_S'))
        chan[nxt].text = f'{pulse_length:.9f}'
        nxt += 1
        chan.insert(nxt, Element('Duty_Cycle'))
        chan[nxt].text = f'{(1.024e-6 + pulse_length) * dopp_prf * prf_broad * presum * 100:.2f}'
        nxt += 1
        chan.insert(nxt, Element('Doppler_PRF_Hz'))
        chan[nxt].text = f'{dopp_prf:.9f}'
        nxt += 1
        chan.insert(nxt, Element('Effective_PRF_Hz'))
        chan[nxt].text = f'{dopp_prf * prf_broad:.9f}'
        nxt += 1
        chan.insert(nxt, Element('Swath_M'))
        # Calculate some ranges for this
        slantMin = altitude / np.sin(near_range * DTR)
        slantMax = altitude / np.sin(far_range * DTR)
        chan[nxt].text = f'{altitude / np.tan(far_range * DTR) - altitude / np.tan(near_range * DTR):.9f}'
        nxt += 1
        chan.insert(nxt, Element('Receive_On_TAC'))
        rec_onTAC = np.floor(2 * slantMin / c0 * TAC + trans_on_tac)
        chan[nxt].text = f'{rec_onTAC:.0f}'
        nxt += 1
        chan.insert(nxt, Element('Receive_Off_TAC'))
        chan[nxt].text = f'{np.ceil(2 * slantMax / c0 * TAC + (pulse_length * TAC) + trans_on_tac):.0f}'
        nxt += 1
        chan.insert(nxt, Element('Transmit_On_TAC'))
        chan[nxt].text = f'{int(trans_on_tac)}'
        nxt += 1
        chan.insert(nxt, Element('Transmit_Off_TAC'))
        chan[nxt].text = f'{trans_on_tac + np.ceil(pulse_length * TAC):.0f}'
        nxt += 1
        chan.insert(nxt, Element('Data_Rate_MBPS'))
        chan[nxt].text = f'{dopp_prf * prf_broad * 2e9 / TAC * rec_onTAC * 4 / 1048576}'
        nxt += 1
        if waveform is not None:
            self.writeWaveform(waveform, wavenumber)
        for key, val in kwargs.items():
            for el in chan.iter():
                loc = el.find(key)
                if loc is not None:
                    loc.text = val
                    continue
            if isinstance(val, str):
                chan.insert(nxt, Element(key))
                chan[nxt].text = val
            elif isinstance(val, (int, float)):
                chan.insert(nxt, Element(key))
                chan[nxt].text = f'{val}'
            elif isinstance(val, dict):
                nxt1 = 0
                for k1, v1 in val.items():
                    if isinstance(val, str):
                        chan[nxt].insert(nxt1, Element(k1))
                        chan[nxt][nxt1].text = v1
                        nxt1 += 1
                    elif isinstance(val, (int, float)):
                        chan[nxt].insert(nxt1, Element(k1))
                        chan[nxt][nxt1].text = f'{v1}'
                        nxt1 += 1
            nxt += 1

        self.n_channels += 1

    def addAntenna(self, ant_num, az_bw, el_bw, dopp_bw, dep_angle, num_az_phase, num_el_phase,
                   is_split=False, squint=0.0):
        """
        Adds an antenna specification to the virtual XML file.
        :param offsets: (float, float, float) Offsets of antenna from gimbal in x, y, z in m.
        :param az_bw: float Azimuth beamwidth in degrees.
        :param el_bw: float Elevation beamwidth in degrees.
        :param dopp_bw: float Doppler beamwidth in degrees.
        :param dep_angle: float Depression angle of antenna in degrees.
        :param squint: float Squint angle in degrees.
        :return: A compliment.
        """
        len_ant_list = len(self.xml.getroot()[2][self._common_channel_insert][9])
        self.xml.getroot()[2][self._common_channel_insert][9].insert(len_ant_list, Element(f'Antenna_{ant_num}'))
        antenna = self.xml.getroot()[2][self._common_channel_insert][9][len_ant_list]
        nxt = 0
        antenna.insert(nxt, Element('Side'))
        antenna[nxt].text = 'Gimballed'
        nxt += 1
        antenna.insert(nxt, Element('Antenna_Squint_Angle_D'))
        antenna[nxt].text = f'{squint}'
        nxt += 1
        antenna.insert(nxt, Element('Azimuth_Beamwidth_D'))
        antenna[nxt].text = f'{az_bw}'
        nxt += 1
        antenna.insert(nxt, Element('Elevation_Beamwidth_D'))
        antenna[nxt].text = f'{el_bw}'
        nxt += 1
        antenna.insert(nxt, Element('Doppler_Beamwidth_D'))
        antenna[nxt].text = f'{dopp_bw}'
        nxt += 1
        antenna.insert(nxt, Element('Antenna_Depression_Angle_D'))
        antenna[nxt].text = f'{dep_angle}'
        nxt += 1
        if num_az_phase is not None:
            antenna.insert(nxt, Element('Num_Azimuth_Phase_Centers'))
            antenna[nxt].text = f'{int(num_az_phase)}'
            nxt += 1
            antenna.insert(nxt, Element('Num_Elevation_Phase_Centers'))
            antenna[nxt].text = f'{int(num_el_phase)}'
            nxt += 1
            antenna.insert(nxt, Element('Is_Split_Weight_Antenna'))
            antenna[nxt].text = 'true' if is_split else 'false'
            nxt += 1
        else:
            antenna.insert(nxt, Element('Num_Azimuth_Phase_Centers'))
            antenna[nxt].text = '1'
            nxt += 1
            antenna.insert(nxt, Element('Num_Elevation_Phase_Centers'))
            antenna[nxt].text = '1'
            nxt += 1
            antenna.insert(nxt, Element('Is_Split_Weight_Antenna'))
            antenna[nxt].text = 'false'
            nxt += 1


    def addPort(self, port_num, offset_x, offset_y, offset_z, cable_length=0., max_gain=None, peak_transmit_power=None):
        len_ant_list = len(self.xml.getroot()[2][self._common_channel_insert][9])
        self.xml.getroot()[2][self._common_channel_insert][9].insert(len_ant_list, Element(f'Antenna_Port_{port_num}'))
        port = self.xml.getroot()[2][self._common_channel_insert][9][len_ant_list]
        port.insert(0, Element('Port_X_Offset_M'))
        port[0].text = f'{offset_x}'
        port.insert(1, Element('Port_Y_Offset_M'))
        port[1].text = f'{offset_y}'
        port.insert(2, Element('Port_Z_Offset_M'))
        port[2].text = f'{offset_z}'
        port.insert(3, Element('Cable_Length_cm'))
        port[3].text = f'{cable_length}'
        if peak_transmit_power is not None:
            port.insert(4, Element('Peak_Transmit_Power_dB'))
            port[4].text = f'{peak_transmit_power}'
        if max_gain is not None:
            port.insert(5, Element('Max_Gain_dB'))
            port[5].text = f'{max_gain}'


    def addGimbal(self, pan_limit, tilt_limit, dep_angle, offsets, attitude, init_course_angle, squint=0.0,
                  update_rate=100, look_side='Left', stabilization_mode='Stripmap'):
        """
        Adds a gimbal to the virtual XML file. Only one is expected.
        :param pan_limit: float Pan limit in degrees.
        :param tilt_limit: float Tilt limit in degrees.
        :param dep_angle: float Depression angle of gimbal in degrees.
        :param offsets: (float, float, float) Offsets of gimbal in x, y, z in m.
        :param attitude: (float, float, float) Initial attitude of gimbal in (roll, pitch, yaw) in degrees.
        :param init_course_angle: float Initial course angle of flight line in radians.
        :param squint: float Squint angle in degrees.
        :param update_rate: float Update rate of gimbal in Hz.
        :param look_side: str Look side of gimbal, either left or right.
        :return: A feeling of accomplishment.
        """
        gimb = self.xml.getroot()[2][4]
        gimb.insert(0, Element('Gimbal_Model'))
        gimb[0].text = self._gimbal_type
        gimb.insert(1, Element('Stabilization_Mode'))
        gimb[1].text = stabilization_mode
        gimb.insert(2, Element('Gimbal_Look_Side'))
        gimb[2].text = look_side
        gimb.insert(3, Element('Pan_Limits_D'))
        gimb[3].text = f'{pan_limit}'
        gimb.insert(4, Element('Tilt_Limits_D'))
        gimb[4].text = f'{tilt_limit}'
        gimb.insert(5, Element('Gimbal_Depression_Angle_D'))
        gimb[5].text = f'{dep_angle}'
        gimb.insert(6, Element('Squint_Angle_D'))
        gimb[6].text = f'{squint}'
        gimb.insert(7, Element('Update_Rate_Hz'))
        gimb[7].text = f'{update_rate}'
        gimb.insert(8, Element('Gimbal_X_Offset_M'))
        gimb[8].text = f'{offsets[0]}'
        gimb.insert(9, Element('Gimbal_Y_Offset_M'))
        gimb[9].text = f'{offsets[1]}'
        gimb.insert(10, Element('Gimbal_Z_Offset_M'))
        gimb[10].text = f'{offsets[2]}'
        gimb.insert(11, Element('Roll_D'))
        gimb[11].text = f'{attitude[0]}'
        gimb.insert(12, Element('Pitch_D'))
        gimb[12].text = f'{attitude[1]}'
        gimb.insert(13, Element('Yaw_D'))
        gimb[13].text = f'{attitude[2]}'
        gimb.insert(14, Element('Initial_Course_Angle_R'))
        gimb[14].text = f'{init_course_angle}'
        gimb.insert(15, Element('Gimbal_Mount_Angle_D'))
        gimb[15].text = '0.00'
        if stabilization_mode == 'Stripmap':
            gimb.insert(16, Element('Stripmap_Settings'))
            gimb[16].insert(0, Element('LPF_Cut-Off_Frequency_Hz'))
            gimb[16][0].text = '10'
    
    def finalize(self, trans_prf, prf_broad=1.5):
        """
        Writes XML to file with pre-loaded values. Also runs GPS/Gimbal to make sure we have enough data.
        :return: str XML file as a string.
        """
        # Finalize some of the common channel settings
        ch_s = self.xml.getroot()[2][self._common_channel_insert]  # Common_Channel_Settings
        ch_s[2].text = f'{int(self.n_cals)}'
        ch_s[4].text = f'{prf_broad:.2f}'
        ch_s[6].text = f'{TAC / trans_prf:.0f}'
        ch_s[7].text = f'{trans_prf:.9f}'
        ch_s[8].text = f'{self.n_channels}'
        indent(self.xml, space="    ", level=0)
        self.xml.write(f'{self._fnme[:-4]}.xml', encoding='UTF-8', xml_declaration=True)
        return tostringlist(self.xml.getroot())

    @property
    def fnme(self):
        return self._fnme


def undulationEGM96(lat, lon):
    if os.name == 'nt':
        egmdatfile = "Z:\\dted\\EGM96.DAT"
    else:
        egmdatfile = "/data5/dted/EGM96.DAT"
    with open(egmdatfile, "rb") as f:
        emg96 = np.fromfile(f, 'double', 1441 * 721, '')
        eg_n = np.ceil(lat / .25) * .25
        eg_s = np.floor(lat / .25) * .25
        eg_e = np.ceil(lon / .25) * .25
        eg_w = np.floor(lon / .25) * .25
        eg1 = emg96[((eg_w + 180 + .25) / .25).astype(int) - 1 + 1441 * ((eg_n + 90 + .25) / .25 - 1).astype(int)]
        eg2 = emg96[((eg_w + 180 + .25) / .25).astype(int) - 1 + 1441 * ((eg_s + 90 + .25) / .25 - 1).astype(int)]
        eg3 = emg96[((eg_e + 180 + .25) / .25).astype(int) - 1 + 1441 * ((eg_n + 90 + .25) / .25 - 1).astype(int)]
        eg4 = emg96[((eg_e + 180 + .25) / .25).astype(int) - 1 + 1441 * ((eg_s + 90 + .25) / .25 - 1).astype(int)]
        egc = (eg2 / ((eg_e - eg_w) * (eg_n - eg_s))) * (eg_e - lon) * (eg_n - lat) + \
              (eg4 / ((eg_e - eg_w) * (eg_n - eg_s))) * (lon - eg_w) * (eg_n - lat) + \
              (eg1 / ((eg_e - eg_w) * (eg_n - eg_s))) * (eg_e - lon) * (lat - eg_s) + \
              (eg3 / ((eg_e - eg_w) * (eg_n - eg_s))) * (lon - eg_w) * (lat - eg_s)
    return egc


def calcCRC(i):
    poly = np.uint32(0xEDB88320)
    CRC = i
    for _ in range(8, 0, -1):
        if CRC & 1:
            CRC = (CRC >> 1) ^ poly
        else:
            CRC >>= 1
    return CRC


def generateCRC(pd):
    CRC = 0
    for dt in pd:
        temp1 = (CRC >> 8) & 0x00FFFFFF
        temp2 = calcCRC((CRC ^ dt) & 0xFF)
        CRC = temp1 ^ temp2
    return np.uint32(CRC)


if __name__ == '__main__':
    fnme = '/data6/SAR_DATA/2023/03302023/Tri_45_Di_25/SAR_03302023_071232.sar'

    output_fnme = '/home/jeff/repo/Raw/tower_redo.sar'

    sdr_f = load(fnme)
    gps = sdr_f.gps_data.sort_values('systime')
    gim = sdr_f.gimbal
    # trumode = getModeValues(int(0x0439010000E00000), not sdr_f.dataversion)

    '''times = gps.index.values
    lats = gps['lat'].values
    lons = gps['lon'].values
    alts = gps['alt'].values
    rolls = gps['r'].values
    pitchs = gps['p'].values
    yaws = gps['y'].values
    vn = gps['vn'].values
    ve = gps['ve'].values
    vu = gps['vu'].values'''

    from simulation_functions import db, enu2llh
    pvec = np.array([0.33216719, 0.06322961, 0.00075249])
    lsline = np.outer(pvec, gps['systime'].values / 125e6).T
    lats, lons, alts = enu2llh(lsline[:, 0], lsline[:, 1], lsline[:, 2], np.array([30.5629, -86.4365, 44.7374]))

    times = gps.index.values
    ve = np.gradient(lsline[:, 0])
    vn = np.gradient(lsline[:, 1])
    vu = np.gradient(lsline[:, 2])
    rolls = gps['r'].values
    pitchs = gps['p'].values
    yaws = gps['y'].values

    sys_to_gps = np.poly1d(np.polyfit(sdr_f.timesync['systime'], sdr_f.timesync['secs'], 1))

    gim_times = sys_to_gps(gim['systime'])

    gimbal = np.array([gim_times, gim['pan'],
                       gim['tilt']]).T

    wr_obj = SDRWrite(output_fnme, int(gps.iloc[0]['gps_wk']), times, lats, lons, alts, rolls, pitchs, yaws, vn, ve, vu,
                      gimbal, settings_alt=88 / 3.2808,
                      settings_vel=50.)

    nrange = [50.5, 48.5]
    frange = [35.5, 37.5]

    for idx, ch in enumerate(sdr_f):
        mode_vals = getModeValues(int(ch.mode, 16), not sdr_f.dataversion)
        real_chirp = np.zeros(ch.pulse_length_N * 2, dtype=np.complex128)
        real_chirp[:ch.pulse_length_N // 2] = np.fft.fft(ch.ref_chirp)[:ch.pulse_length_N // 2]
        real_chirp[-ch.pulse_length_N // 2:] = np.fft.fft(ch.ref_chirp)[ch.pulse_length_N // 2:]
        real_chirp = np.fft.ifft(real_chirp).real
        wr_obj.addChannel(ch.fc, ch.bw,
                          ch.pulse_length_S, ch.prf / 1.5, nrange[idx],
                          frange[idx], 90.00,
                          31, trans_on_tac=1000, att_mode='Static', rec_only=False, rec_slice='A', prf_broad=1.5,
                          digital_channel=mode_vals['digital_channel'], wavenumber=mode_vals['wavenumber'],
                          adc_channel=mode_vals['adc_channel'], receiver_slot=mode_vals['receiver_slot'],
                          receiver_channel=mode_vals['receiver_channel'], upconverter_slot=mode_vals['upconverter_slot'],
                          select_89=mode_vals['8/9_select'], band=mode_vals['band'], dac_channel=mode_vals['dac_channel'],
                          ddc_enable=mode_vals['ddc_enable'], filter_select=mode_vals['filter_select'],
                          rx_port=mode_vals['rx_port'], tx_port=mode_vals['tx_port'], polarization=mode_vals['polarization'],
                          numconsmodes=mode_vals['numconsmodes'], awg_enable=mode_vals['awg_enable'],
                          rf_ref_wavenumber=mode_vals['rf_ref_wavenumber'],
                          offset_video=5e6, waveform=real_chirp)

        for fr in tqdm(np.arange(ch.ncals)):
            pulse = sdr_f.getPulse(fr, idx, is_cal=True)
            if pulse is not None:
                time = sys_to_gps(ch.cal_time[fr])
                wr_obj.writePulse(time, 31, pulse[:1766], idx, True)
        for fr in tqdm(np.arange(ch.nframes)):
            pulse = sdr_f.getPulse(fr)
            if pulse is not None:
                time = ch.pulse_time[fr]
                att = ch.atts[fr]
                wr_obj.writePulse(time, att, pulse[:1766], idx, False)

    print(hex(wr_obj._channel_mode[0]))
    chmode = getModeValues(wr_obj._channel_mode[0])

    for idx, a in enumerate(sdr_f.ant):
        wr_obj.addAntenna(idx, a.az_bw / DTR, a.el_bw / DTR, a.dopp_bw / DTR, a.dep_ang / DTR,
                          a.az_phase_centers, a.el_phase_centers,
                          False, a.squint)

    for idx, port in enumerate(sdr_f.port):
        if not isinstance(port, list):
            wr_obj.addPort(idx, port.x, port.y, port.z, cable_length=0., max_gain=25, peak_transmit_power=100)

    wr_obj.addGimbal(15., 15., 43., [.0203, .2029, -.3585], [0., 0., 90.], 1.57562403, squint=0.0,
                  update_rate=100, look_side='Right')

    wr_obj.finalize(46957.25)

    testparse = SDRParse(output_fnme)

    plt.figure('GPS')
    plt.subplot(2, 2, 1)
    plt.title('lat')
    plt.plot(gps['lat'] - testparse.gps_data['lat'])
    plt.subplot(2, 2, 2)
    plt.title('lon')
    plt.plot(gps['lon'] - testparse.gps_data['lon'])
    plt.subplot(2, 2, 3)
    plt.title('alt')
    plt.plot(gps['alt'] - testparse.gps_data['alt'])

    plt.figure()
    plt.plot(db(np.fft.fft(testparse[0].ref_chirp)))
    plt.plot(db(np.fft.fft(sdr_f[0].ref_chirp)))

    plt.figure('Gimbal')
    plt.subplot(2, 1, 1)
    plt.title('Pan')
    plt.plot(gimbal[:, 0] - testparse.gimbal['pan'])
    plt.subplot(2, 1, 2)
    plt.title('Tilt')
    plt.plot(gimbal[:, 1] - testparse.gimbal['tilt'])

    plt.figure('Packet Timings')
    plt.scatter(sdr_f.gimbal['systime'], np.ones(sdr_f.gimbal.shape[0]) + 2, c='b')
    plt.scatter(sdr_f[0].cal_time, np.ones(len(sdr_f[0].cal_time)) - 1, c='r')
    plt.scatter(sdr_f[0].sys_time, np.ones(len(sdr_f[0].sys_time)), c='g')
    plt.legend(['Gimbal', 'Cal', 'Sys'])

    plt.figure('Packet Timings 2')
    plt.scatter(testparse.gimbal['systime'], np.ones(testparse.gimbal.shape[0]) + 2, c='b')
    plt.scatter(testparse[0].cal_time, np.ones(len(testparse[0].cal_time)) - 1, c='r')
    plt.scatter(testparse[0].sys_time, np.ones(len(testparse[0].sys_time)), c='g')
    plt.legend(['Gimbal', 'Cal', 'Sys'])

