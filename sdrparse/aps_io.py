import os
import struct

import numpy as np
from xml.dom.minidom import parse
from re import search
import collections.abc as collections
import mmap

from tqdm import tqdm


def getRawDataGen(filename, num_pulses, num_desired_frames=None, start_pulse=0, isIQ=False):
    """
    Python generator to parse raw data from an APS debug .dat file.
    :param filename: str Name of .dat file to parse.
    :param num_pulses: int Number of pulses to parse in each iteration of the generator.
    :param num_desired_frames: int Total number of pulses to parse.
    :param start_pulse: int The function will start with this pulse number.
    :param isIQ: bool if True, assumes data is stored as complex numbers. Otherwise, reads data as ints.
    :return:
        raw_data: numpy array Array of pulse data, size of number_samples_per_pulse x num_pulses.
        pulse_range: numpy array List of each pulse's number in the parsed file.
        attenuation: numpy array List of attenuation factors associated with each pulse.
        sys_time: numpy array List of system times, in TAC, associated with each pulse.
    """
    with open(filename, 'rb') as fid:
        num_frames = np.fromfile(fid, 'uint32', 1, '')[0]
        if isIQ:
            num_samples = np.fromfile(fid, 'uint32', 1, '')[0]
        else:
            num_samples = np.fromfile(fid, 'uint16', 1, '')[0]
        attenuation = np.fromfile(fid, 'int8', num_frames, '')
        sys_time = np.fromfile(fid, 'double', num_frames, '')
        if num_desired_frames is None:
            ndf = num_frames
        else:
            ndf = num_desired_frames
        if isIQ:
            fid.seek(fid.tell() + start_pulse * 2 * num_samples)
            for npulse in range(0, ndf, num_pulses):
                proc_pulses = num_pulses if npulse + num_pulses < ndf else ndf - npulse
                raw_data = np.zeros((num_samples, proc_pulses)).astype(np.complex64)
                pulse_range = np.arange(npulse + start_pulse, npulse + proc_pulses + start_pulse)
                for i in range(proc_pulses):
                    tmp = np.fromfile(fid, 'int16', num_samples * 2, '')
                    raw_data[:, i] = (tmp[0::2] + 1j * tmp[1::2]) * 10 ** (attenuation[pulse_range[0] + i] / 20)
                yield raw_data, pulse_range, attenuation[pulse_range], sys_time[pulse_range]
        else:
            fid.seek(fid.tell() + start_pulse * 2 * num_samples)
            for npulse in range(0, ndf, num_pulses):
                proc_pulses = num_pulses if npulse + num_pulses < ndf else ndf - npulse
                raw_data = np.zeros((num_samples, proc_pulses)).astype(np.int16)
                pulse_range = np.arange(npulse + start_pulse, npulse + proc_pulses + start_pulse)
                for i in range(proc_pulses):
                    raw_data[:, i] = np.fromfile(fid, 'int16', num_samples, '') * 10 ** (
                            attenuation[pulse_range[0] + i] / 20)
                yield raw_data, pulse_range, attenuation[pulse_range], sys_time[pulse_range]


def getRawData(filename, num_pulses=None, start_pulse=0):
    """
    Parses raw data from an APS debug .dat file.
    :param filename: str Name of .dat file to parse.
    :param num_pulses: int Number of pulses to parse.
    :param start_pulse: int The function will start with this pulse number.
    :param isIQ: bool if True, assumes data is stored as complex numbers. Otherwise, reads data as ints.
    :return:
        raw_data: numpy array Array of pulse data, size of number_samples_per_pulse x num_pulses.
        pulse_range: numpy array List of each pulse's number in the parsed file.
        attenuation: numpy array List of attenuation factors associated with each pulse.
        sys_time: numpy array List of system times, in TAC, associated with each pulse.
    """
    with open(filename, 'r+b') as fid:
        mm = mmap.mmap(fid.fileno(), 0)
        headerData = mm.read(8)
        recAntNum, recAntPortNum, recChanNum = np.frombuffer(headerData, 'uint8', 3)
        numSamples = np.frombuffer(headerData, 'uint32', 1, 3).item()
        isComplex = np.frombuffer(headerData, 'uint8', 1, 7).item()
        sampleSize = 8 if isComplex else 4
        dataType = 'complex64' if isComplex else 'float32'
        frameSize = 4 + 8 + numSamples * sampleSize
        nframes = (mm.size() - 64) / frameSize if num_pulses is None else num_pulses
        frame_num = np.zeros(nframes)
        sys_time = np.zeros_like(frame_num)
        raw_data = np.zeros((numSamples, nframes), dtype=dataType)
        mm.seek(8 + frameSize * start_pulse)
        for n in range(nframes):
            dt = mm.read(frameSize)
            frame_num[n] = np.frombuffer(dt, count=1, dtype='uint32')
            sys_time[n] = np.frombuffer(dt, count=1, offset=4, dtype='double')
            raw_data[:, n] = np.frombuffer(dt, count=numSamples, offset=12, dtype=dataType)
    return recAntNum, recAntPortNum, recChanNum, nframes, numSamples, frame_num, sys_time, raw_data


def getRawDataHeader(filename):
    """
    Finds and returns header information for all pulses in an APS debug .dat raw data file.
    :param filename: str Name of APS debug .dat raw file to parse.
    :return:
        numFrames: int Number of pulses in .dat file.
        numSamples: int Number of samples in each pulse of the file.
        attenuation: numpy array List of length numFrames with attenuation number of each pulse in the file.
        sys_time: numpy array List of length numFrames with system times, in TAC, of each pulse in the file.
    """
    with open(filename, 'r+b') as fid:
        mm = mmap.mmap(fid.fileno(), 0)
        headerData = mm.read(8)
        recAntNum, recAntPortNum, recChanNum = np.frombuffer(headerData, 'uint8', 3)
        numSamples = np.frombuffer(headerData, 'uint32', 1, 3).item()
        isComplex = np.frombuffer(headerData, 'uint8', 1, 7).item()
        sampleSize = 8 if isComplex else 4
        dataType = 'complex64' if isComplex else 'float32'
        frameSize = 4 + 8 + numSamples * sampleSize
        nframes = int((mm.size()) / frameSize)
        frame_num = np.zeros(nframes)
        sys_time = np.zeros_like(frame_num)
        for n in tqdm(range(nframes)):
            dt = mm.read(frameSize)
            frame_num[n] = np.frombuffer(dt, count=1, dtype='uint32').item()
            sys_time[n] = np.frombuffer(dt, count=1, offset=4, dtype='double').item()
    return recAntNum, recAntPortNum, recChanNum, nframes, numSamples, frame_num, sys_time


def getBasebandCenterFrequency(band, fc, nco_freq, is_upper_band=False):
    """
    Helper function. Calculates the basebanded center frequency from XML parameters.
    :param band: str Name of frequency band in XML. Either 'X-Band' or 'Ka-Band'.
    :param fc: float Center frequency of signal before basebanding.
    :param nco_freq: float NCO frequency from XML file.
    :param is_upper_band: bool If True, adds 1 GHz to offset.
    :return: float Basebanded center frequency
    """
    bboffset = 8e9 if band == 'X-Band' else 33e9
    bboffset += 1e9 if is_upper_band else 0
    return (fc - bboffset - abs(nco_freq)) % 2e9


def loadGPSData(filename, prep_for_real_time=True):
    """
    Parses and returns a dict comprised of GPS data from APS debug .dat files.
    :param filename: str Name of APS debug output .dat file.
    :return: dict A dictionary with GPS information parsed from APS debug .dat files.
    """
    if prep_for_real_time:
        with open(filename, 'rb') as fid:
            frames = 0
            ret_dict = {'lat': [], 'lon': [], 'alt': [], 'vn': [], 'vu': [], 've': [], 'r': [],
                        'p': [], 'az': [],
                        'gps_ms': [], 'systime': []}
            while fid.tell() + 92 <= os.path.getsize(filename):
                ret_dict['gps_ms'].append(np.fromfile(fid, 'double', 1)[0])
                pps_time = np.fromfile(fid, 'uint32', 1)[0]
                ret_dict['systime'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['lon'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['lat'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['alt'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['ve'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['vn'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['vu'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['r'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['p'].append(np.fromfile(fid, 'double', 1)[0])
                ret_dict['az'].append(np.fromfile(fid, 'double', 1)[0])
                frames += 1
        for key, val in ret_dict.items():
            ret_dict[key] = np.array(val)
        ret_dict['frames'] = frames
    else:
        with open(filename, 'rb') as fid:
            numFrames = np.fromfile(fid, 'int32', 1, '')[0]
            ret_dict = {'frames': numFrames, 'lat': np.fromfile(fid, 'float64', numFrames, ''),
                        'lon': np.fromfile(fid, 'float64', numFrames, ''),
                        'alt': np.fromfile(fid, 'float64', numFrames, ''),
                        'vn': np.fromfile(fid, 'float64', numFrames, ''),
                        've': np.fromfile(fid, 'float64', numFrames, ''),
                        'vu': np.fromfile(fid, 'float64', numFrames, ''),
                        'r': np.fromfile(fid, 'float64', numFrames, ''),
                        'p': np.fromfile(fid, 'float64', numFrames, ''),
                        'azimuthX': np.fromfile(fid, 'float64', numFrames, ''),
                        'azimuthY': np.fromfile(fid, 'float64', numFrames, ''),
                        'gps_ms': np.fromfile(fid, 'float64', numFrames, ''),
                        'systime': np.fromfile(fid, 'float64', numFrames, '')}
    return ret_dict


def loadCorrectionGPSData(filename, post=True, prep_for_real_time=True):
    if post:
        if prep_for_real_time:
            with open(filename, 'rb') as fid:
                numFrames = np.fromfile(fid, '<i4', 1, '')[0]
                ret_dict = {'frames': numFrames, 'systime': np.fromfile(fid, '<d', numFrames, ''),
                            'tx_pos': np.fromfile(fid, '<d', numFrames * 3, '').reshape(numFrames, 3),
                            'rx_pos': np.fromfile(fid, '<d', numFrames * 3, '').reshape(numFrames, 3),
                            'point_vec': np.fromfile(fid, '<c8', numFrames, '')}
    else:
        if prep_for_real_time:
            with open(filename, 'rb') as fid:
                numFrames = np.fromfile(fid, '<u4', 1, '')[0]
                ret_dict = {'frames': numFrames, 'lat': np.fromfile(fid, '<d', numFrames, ''),
                            'lon': np.fromfile(fid, '<d', numFrames, ''),
                            'alt': np.fromfile(fid, '<d', numFrames, ''),
                            'vn': np.fromfile(fid, '<d', numFrames, ''),
                            've': np.fromfile(fid, '<d', numFrames, ''),
                            'gps_ms': np.fromfile(fid, '<d', numFrames, '')}
    return ret_dict


def loadGimbalData(filename, is_interp=True, prep_for_real_time=True):
    """
    Parses and returns a dict comprised of gimbal data from APS debug .dat files.
    :param filename: str Name of APS debug .dat file to parse gimbal data from.
    :return: dict A dictionary with gimbal information parsed from APS debug .dat files.
    """
    if is_interp:
        if prep_for_real_time:
            with open(filename, 'rb') as fid:
                numFrames = np.fromfile(fid, '<u4', 1, '')[0]
                ret_dict = {'frames': numFrames, 'gps_ms': np.fromfile(fid, '<d', numFrames, ''),
                            'pan': np.fromfile(fid, '<d', numFrames, ''),
                            'tilt': np.fromfile(fid, '<d', numFrames, ''),
                            'systime': np.fromfile(fid, '<d', numFrames, '')}
    else:
        if prep_for_real_time:
            with open(filename, 'rb') as fid:
                frames = 0
                ret_dict = {'pan': [], 'tilt': [], 'systime': []}
                while fid.tell() + 24 <= os.path.getsize(filename):
                    ret_dict['systime'].append(np.fromfile(fid, 'double', 1)[0])
                    ret_dict['pan'].append(np.fromfile(fid, 'double', 1)[0])
                    ret_dict['tilt'].append(np.fromfile(fid, 'double', 1)[0])
                    frames += 1
            for key, val in ret_dict.items():
                ret_dict[key] = np.array(val)
            ret_dict['frames'] = frames
        else:
            with open(filename, 'rb') as fid:
                numFrames = np.fromfile(fid, 'int32', 1, '')[0]
                ret_dict = {'pan': np.fromfile(fid, 'float64', numFrames, ''),
                            'tilt': np.fromfile(fid, 'float64', numFrames, ''),
                            'systime': np.fromfile(fid, 'float64', numFrames, '')}
    return ret_dict


def loadReferenceChirp(filename):
    """
    Creates a reference chirp array from APS debug .dat files.
    :param filename: str Name of APS debug .dat file to parse reference chirp data from.
    :return: numpy array Complex array of length num_samples of reference chirp data.
    """
    with open(filename, 'rb') as fid:
        num_samples = np.fromfile(fid, 'uint32', 1, '')[0]
        tmp = np.fromfile(fid, 'complex64', num_samples, '')
        ret = tmp * 10 ** (31 / 20)
    return ret


def loadMatchedFilter(filename):
    """
    Creates a matched filter array from APS debug .dat files.
    :param filename: str Name of APS debug .dat file to parse matched filter data from.
    :return: numpy array Complex array of length num_samples of matched filter data.
    """
    with open(filename, 'rb') as fid:
        FFT_length = np.fromfile(fid, 'uint32', 1, '')[0]
        ret = np.fromfile(fid, 'complex64', FFT_length, '')
    return ret


def loadXMLFile(fnme, keep_structure=False):
    """
    Parse the xml header file ('ash') associated with the SAR image data to
    retrieve the relative parameters for the dataset
    """
    # instantiate an object of the W3C Document Object Model (DOM) using the ".ash" file for parsing the xml
    # The subapertures have a different name than full aperture SAR images
    dom = parse_xml(parse(fnme))
    try:
        data = dom['SlimSAR_Configuration']['SlimSAR_Info']
    except:
        data = dom['SlimSDR_Configuration']['SlimSDR_Info']
    if keep_structure:
        return dom
    else:
        return flatten(data)


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def parse_xml(xmlObject, param={}):
    """Extract and return key/value pairs from xml objects.  This method accepts a DOM xml object"""
    # Loop through each node of the DOM xml object and assign the key/value pairs to the dictionary
    for node in xmlObject.childNodes:
        # if the type of node is an element node, then try to extract the key/value
        if node.nodeType == node.ELEMENT_NODE:
            if sum([n.nodeType == n.TEXT_NODE for n in node.childNodes]) == 1:
                # if it has a child node, then we will extract the data
                if node.hasChildNodes():
                    # if the node value contains a letter A-Za-z then interpret it as a string
                    if search('[A-Za-z]|:', node.childNodes[0].nodeValue):
                        param[str(node.nodeName)] = str(node.childNodes[0].nodeValue)
                    # else interpret the node value as a float
                    else:
                        try:
                            param[str(node.nodeName)] = float(node.childNodes[0].nodeValue)
                        except ValueError:
                            param[str(node.nodeName)] = node.childNodes[0].nodeValue
                # else, if there is not a child node, then assign it an empty list
                else:
                    param[str(node.nodeName)] = []
            else:
                param[str(node.nodeName)] = parse_xml(node, {})
    return param


def window_taylor(N, nbar=4, sll=-30):
    """Taylor tapering window
    Taylor windows allows you to make tradeoffs between the
    mainlobe width and sidelobe level (sll).
    Implemented as described by Carrara, Goodman, and Majewski
    in 'Spotlight Synthetic Aperture Radar: Signal Processing Algorithms'
    Pages 512-513
    :param N: window length
    :param float nbar:
    :param float sll:
    The default values gives equal height
    sidelobes (nbar) and maximum sidelobe level (sll).
    .. warning:: not implemented
    .. seealso:: :func:`create_window`, :class:`Window`
    """
    B = 10 ** (-sll / 20)
    A = np.log(B + np.sqrt(B ** 2 - 1)) / np.pi
    s2 = nbar ** 2 / (A ** 2 + (nbar - 0.5) ** 2)
    ma = np.arange(1, nbar)

    def calc_Fm(m):
        numer = (-1) ** (m + 1) \
                * np.prod(1 - m ** 2 / s2 / (A ** 2 + (ma - 0.5) ** 2))
        denom = 2 * np.prod([1 - m ** 2 / j ** 2 for j in ma if j != m])
        return numer / denom

    Fm = np.array([calc_Fm(m) for m in ma])

    def W(n):
        return 2 * np.sum(
            Fm * np.cos(2 * np.pi * ma * (n - N / 2 + 1 / 2) / N)) + 1

    w = np.array([W(n) for n in range(N)])
    # normalize (Note that this is not described in the original text)
    scale = W((N - 1) / 2)
    w /= scale
    return w
